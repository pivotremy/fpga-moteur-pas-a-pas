
### Bonjour, je m'appelle  Pivot Remy LOUA  👋

## Je suis étudiant en 2ème année du Cycle Ingénieur de la filière Instrumentation - Systèmes Embarqués à Sup Galilée, École d’ingénieurs de l'Institut Galilée!! 

- 🌱 J'apprends tout en ce moment 🤣
- 🥅 Objectifs 2021 : Contribuer aux projets Open Source.
- ⚡ Fait amusant       : J'aime participer au hackathon.

### FPGA - Moteur pas à pas
Contrôler un moteur numérique du type pas à pas, sens de rotation, vitesse de rotation, contrôle du
pas en mode local par potentiomètre numérique et à distance avec une liaison USB-série.
Développement du contrôle en local avec des encodeurs numériques et ajout dans le module de communication UART (la liaison série asynchrone), pour charger les données à distance via un PC (interface graphique en python).
