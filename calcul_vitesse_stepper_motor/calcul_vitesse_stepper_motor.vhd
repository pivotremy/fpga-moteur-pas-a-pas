--Ce code est une possibilitée de calcul de la Vitesse du moteur mais il y en a sans doute d’autres—
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;

--en fonction de la fréquence d’entrée du moteur entre 0-255Hz on calcul la Vitesse en tr/mn--
entity calcul_vitesse_stepper_motor is
Port (
    frequency_in_motor       : in STD_LOGIC_VECTOR (7 downto 0); --entre 0 et 255Hz--
    select_freq_or_vitesse   : in STD_LOGIC;
    affichage_vitesse_digit0 : out STD_LOGIC_VECTOR (3 downto 0);
    affichage_vitesse_digit1 : out STD_LOGIC_VECTOR (3 downto 0);
    affichage_vitesse_digit2 : out STD_LOGIC_VECTOR (3 downto 0);
    affichage_vitesse_digit3 : out STD_LOGIC_VECTOR (3 downto 0));
end calcul_vitesse_stepper_motor;

architecture Behavioral of calcul_vitesse_stepper_motor is
    signal Frequence                     : INTEGER range 0 to 255;
    signal N                             : INTEGER; -- Tours/minutes ou *60 tr/heures
    signal Q                             : STD_LOGIC_VECTOR (11 downto 0);
    signal Q2                            : STD_LOGIC_VECTOR (7 downto 0);
    signal decalage_a_gauche             : INTEGER;
    signal view_signal_decalage_a_gauche : STD_LOGIC_VECTOR (26 downto 0);
    signal view_signal_N                 : STD_LOGIC_VECTOR (26 downto 0);
begin
    Frequence <= conv_integer(frequency_in_motor);
    decalage_a_gauche <= (Frequence * 100000); -- car on ne peut que declarer des entiers pas des nombres flottants, on décale à gauche le resultat pour conserver la valeur entière--
    N <= (decalage_a_gauche / 4096) * 60; --tours/s -->tours/minutes @100Hz 1.464 tr/min
    Q <= std_logic_vector(to_unsigned(N/100, 12)); -- on converti en std_logic_vector le nombre--
    Q2 <= frequency_in_motor;

    process(Q,Q2,select_freq_or_vitesse) --liste de sensibilité, signaux d’entrées ou internes---
        -- variable temporaire
        variable temp : STD_LOGIC_VECTOR ( 11 downto 0 ); --utilisé pour la valeur da la vitesse--
        variable temp2 : STD_LOGIC_VECTOR ( 7 downto 0 ); --utilisé pour la valeur de fréquence-- variable bcd : STD_LOGIC_VECTOR ( 15 downto 0 ); -- variable de sortie BCD vers l’affichage--
    begin
            --Code calcul de la Vitesse du moteur en tr/min suite — Le double dabble
            -- 2 4 3 --- valeur à afficher
            -- 0010 0100 0011 -- valeur binaire
            -- <---------ORIGINAL
            --000 0000 0000 11110011 b243 en base binaire
            --Le double dabble est un algorithme utilisé pour convertir des nombres d'un système--
            --binaire vers un système décimal. Pour des raisons pratiques, le résultat est--
            --généralement stocké sous la forme de décimal codé en binaire (BCD)--
            --En partant du registre initial, l'algorithme effectue n itérations (soit 8 dans l'exemple ci-dessous)
            --a chaque itération, le registre est décalé d'un bit vers la gauche. Avant d'effectuer cette opération,
            --la partie au format BCD est analysée, décimale par décimale. Si une décimale en BCD (4 bits)
            --est plus grande que 4 alors on lui ajoute 3. Cette incrément permet de s'assurer qu'une valeur de 5 après incrémentation et décalage, devient 16 et se propage correctement à la décimale suivante.
            --- 0000 0000 0000 11110011 Initialisation
            --- 0000 0000 0001 11100110 Décalage
            --- 0000 0000 0011 11001100 Décalage
            --- 0000 0000 0111 10011000 Décalage
            --- 0000 0000 1010 10011000 Ajouter 3 à la première décimale BCD, puisque sa valeur était 7
            --- 0000 0001 0101 00110000 Décalage
            --- 0000 0001 1000 00110000 Ajouter 3 à la première décimale BCD, puisque sa valeur était 5
            --- 0000 0011 0000 01100000 Décalage
            --- 0000 0110 0000 11000000 Décalage
            --- 0000 1001 0000 11000000 Ajouter 3 à la seconde décimale BCD, puisque sa valeur était 6
            --- 0001 0010 0001 10000000 Décalage
            -- 0010 0100 0011 00000000 Décalage
            --2----4---3-- résultat obtenu à afficher
            --mettre à zéro la variable bcd
        bcd := (others => '0');
        if select_freq_or_vitesse ='1' then -- select bit pour afficher soit la fréquence sur les afficheurs 4 digits annexes ou la Vitesse en tr/mn--
            temp (11 downto 0) := Q ;-- lire le signal Q dans la variable
            for i in 0 to 11 loop --- boucle d’itération—affichage sur 12 bits
                if bcd(3 downto 0) > 4 then
                    bcd(3 downto 0) := bcd(3 downto 0) + 3;
                end if;
                if bcd(7 downto 4) > 4 then
                    bcd(7 downto 4) := bcd(7 downto 4) + 3;
                end if;
                if bcd(11 downto 8) > 4 then
                    bcd(11 downto 8) := bcd(11 downto 8) + 3;
                end if;
                bcd := bcd(14 downto 0) & temp(11);
                temp := temp(10 downto 0) & '0'; -- decalage d’un bit à gauche--
            end loop;
        elsif select_freq_or_vitesse ='0' then -- si bit de selection passe à 0--
            temp2(7 downto 0) := Q2 ;
            --- boucle d’itération—affichage sur 8 bits
            for i in 0 to 7 loop -- car on souhaite afficher sur 8 bits la fréquence--
                if bcd(3 downto 0) > 4 then
                    bcd(3 downto 0) := bcd(3 downto 0) + 3;
                end if;
                if bcd(7 downto 4) > 4 then
                    bcd(7 downto 4) := bcd(7 downto 4) + 3;
                end if;
                if bcd(11 downto 8) > 4 then
                    bcd(11 downto 8) := bcd(11 downto 8) + 3;
                end if;
                bcd := bcd(14 downto 0) & temp2(7);
                temp2 := temp2(6 downto 0) & '0'; -- decalage d’un bit à gauche--
            end loop;
        end if;
            -- set outputs→ vers le multiplexeur et 7 segments---
        affichage_vitesse_digit0 <= STD_LOGIC_VECTOR (bcd(3 downto 0));
        affichage_vitesse_digit1 <= STD_LOGIC_VECTOR (bcd(7 downto 4));
        affichage_vitesse_digit2 <= STD_LOGIC_VECTOR (bcd(11 downto 8));
        affichage_vitesse_digit3 <= STD_LOGIC_VECTOR (bcd(15 downto 12));
    end process;
end Behavioral;