# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 08:37:28 2021

@author: pivot
"""

import serial
import struct
import time

ser =serial.Serial(
port ='COM5',
baudrate = 9600,
parity = serial.PARITY_NONE,
stopbits = serial.STOPBITS_ONE,
bytesize = serial.EIGHTBITS,
timeout = 100
)

def packUnsignedCharAsUChar(valeur):
 """Packs a python 1 byte unsigned char"""
 return struct.pack('B', valeur) #should check bounds

def compteur1(): # compteur 1 pour rampe montante
 i = 1
 while i <= 255:
     time.sleep(0.05)
     ser.write(packUnsignedCharAsUChar(i)) #format de données envoyer (1byte)
     print(i)
     i = i + 1
     if i == 255:
         compteur2()

def compteur2(): # compteur 1 pour rampe descendante
 i = 255
 while i >= 0:
     time.sleep(0.01) # délais entre chaque valeur envoyée ici 10ms
     ser.write(packUnsignedCharAsUChar(i)) #format de données envoyer (1byte)
     print(i)
     i = i - 1
     if i == 0:
         compteur1()

while True: # boucle tant que vrai
    compteur1()