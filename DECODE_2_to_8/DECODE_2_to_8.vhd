library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DECODE_2_to_8 is
    Port ( sel : in  STD_LOGIC_VECTOR (2 downto 0);
        select_freq_or_vitesse : in std_logic;
        DP1 : out  STD_LOGIC;
        afficheur_0 : out  STD_LOGIC;
        afficheur_1 : out  STD_LOGIC;
        afficheur_2 : out  STD_LOGIC;
        afficheur_3 : out  STD_LOGIC;
        afficheur_4 : out  STD_LOGIC;
        afficheur_5 : out  STD_LOGIC;
        afficheur_6 : out  STD_LOGIC;
        afficheur_7 : out  STD_LOGIC);
end DECODE_2_to_8;

architecture Behavioral of DECODE_2_to_8 is

begin
    process(sel,select_freq_or_vitesse)
    begin
        afficheur_0 <='1'; DP1 <= '1'; afficheur_1 <= '1'; afficheur_2 <='1'; afficheur_3 <= '1'  ;
        afficheur_4 <='1'; afficheur_5 <='1'; afficheur_6 <='1' ; afficheur_7 <='1';
        if select_freq_or_vitesse='1' then
            case sel is
                    --when "000" => afficheur_0 <= '0; DP1 <= '1';
                    --- code a completer
                when others => afficheur_0 <='0'; DP1 <='1';
            end case;
        else
            case sel is
                    --when "000" => afficheur_0 <= '0; DP1 <= '1';
                    --- code a completer
                    when others => afficheur_0 <='0'; DP1 <='1';
            end case;
        end if;
    end process;
end Behavioral;