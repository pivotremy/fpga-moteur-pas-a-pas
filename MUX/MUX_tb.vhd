LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY MUX_tb IS
END MUX_tb;
 
ARCHITECTURE behavior OF MUX_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MUX
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         C : IN  std_logic_vector(3 downto 0);
         D : IN  std_logic_vector(3 downto 0);
         E : IN  std_logic_vector(3 downto 0);
         F : IN  std_logic_vector(3 downto 0);
         G : IN  std_logic_vector(3 downto 0);
         H : IN  std_logic_vector(3 downto 0);
         SEL : IN  std_logic_vector(2 downto 0);
         sortie_mux : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal C : std_logic_vector(3 downto 0) := (others => '0');
   signal D : std_logic_vector(3 downto 0) := (others => '0');
   signal E : std_logic_vector(3 downto 0) := (others => '0');
   signal F : std_logic_vector(3 downto 0) := (others => '0');
   signal G : std_logic_vector(3 downto 0) := (others => '0');
   signal H : std_logic_vector(3 downto 0) := (others => '0');
   signal SEL : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal sortie_mux : std_logic_vector(3 downto 0);

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MUX PORT MAP (
          A => A,
          B => B,
          C => C,
          D => D,
          E => E,
          F => F,
          G => G,
          H => H,
          SEL => SEL,
          sortie_mux => sortie_mux
        );

   -- Clock process definitions
   SEL_process : process
   begin
		SEL <= "000";
		wait for 2 ns;
		SEL <= "001";
		wait ;
   end process;
	
	A_process : process
   begin
		A <= "0000";
		wait for 2 ns;
		A <= "1111";
		wait ;
   end process;
	B_process : process
   begin
		B <= "0101";
		wait for 2 ns;
		B <= "1011";
		wait ;
   end process;
		C_process : process
   begin
		C <= "0111";
		wait for 2 ns;
		C <= "1010";
		wait for 5 ns;
   end process;
		D_process : process
   begin
		D <= "0111";
		wait for 2 ns;
		D <= "0011";
		wait for 5 ns;
   end process;
		E_process : process
   begin
		E <= "0101";
		wait for 2 ns;
		E <= "1010";
		wait ;
   end process;
			F_process : process
   begin
		F <= "0111";
		wait for 2 ns;
		F <= "1011";
		wait for 10 ns;
   end process;
			G_process : process
   begin
		G <= "0101";
		wait for 2 ns;
		G <= "1011";
		wait ;
   end process;
				H_process : process
   begin
		H <= "0101";
		wait for 2 ns;
		H <= "1011";
		wait ;
   end process;
END;