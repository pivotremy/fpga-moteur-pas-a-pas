library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity MUX is
    Port ( A   : in  STD_LOGIC_VECTOR (3 downto 0);
           B   : in  STD_LOGIC_VECTOR (3 downto 0);
           C   : in  STD_LOGIC_VECTOR (3 downto 0);
           D   : in  STD_LOGIC_VECTOR (3 downto 0);
           E   : in  STD_LOGIC_VECTOR (3 downto 0);
           F   : in  STD_LOGIC_VECTOR (3 downto 0);
           G   : in  STD_LOGIC_VECTOR (3 downto 0);
           H   : in  STD_LOGIC_VECTOR (3 downto 0);
           SEL : in  STD_LOGIC_VECTOR (2 downto 0);
           sortie_mux : out  STD_LOGIC_VECTOR (3 downto 0));
end MUX;

architecture Behavioral of MUX is

begin
    process(sel,A,B,C,D,E,F,G,H)
        begin 
            case sel is
                when "001"  => sortie_mux <= B;
                when "010"  => sortie_mux <= C;
                when "011"  => sortie_mux <= D;
                when "101"  => sortie_mux <= E;
                when "100"  => sortie_mux <= F;
                when "111"  => sortie_mux <= G;
                when "110"  => sortie_mux <= H;
                when others => sortie_mux <= A;
            end case;
    end process;
end Behavioral;