from tkinter import *
import serial
import struct
import time

#-------------------------------------------------------
#						PROGRAMME
#-------------------------------------------------------

ser = serial.Serial(
port ='COM5',
baudrate = 9600,
parity = serial.PARITY_NONE,
stopbits = serial.STOPBITS_ONE,
bytesize = serial.EIGHTBITS,
timeout = 100
)


def packUnsignedCharAsUChar(valeur):
 """Packs a python 1 byte unsigned char"""
 return struct.pack('B', valeur) #should check bounds

def compteur1(): # compteur 1 pour rampe montante
 i = 1
 while i <= 255:
     time.sleep(0.05)
     ser.write(packUnsignedCharAsUChar(i)) #format de données envoyer (1byte)
     print(i)
     i = i + 1
     if i == 255:
         compteur2()

def compteur2(): # compteur 1 pour rampe descendante
 i = 255
 while i >= 0:
     time.sleep(0.01) # délais entre chaque valeur envoyée ici 10ms
     ser.write(packUnsignedCharAsUChar(i)) #format de données envoyer (1byte)
     print(i)
     i = i - 1
     if i == 0:
         compteur1()


fenetre = Tk()
fenetre.title("Commande de moteur pas à pas P.Remy LOUA 2021 Projet FPGA")
fenetre.geometry("800x500")
#fenetre['bg']='grey'

def print_values():
    print(my_com.get(), my_baud.get())
def cycle_1s():
    # On arrive ici toutes les 1000 ms.
    heure.set(time.strftime('%H:%M:%S'))
    fenetre.after(1000,cycle_1s)
def fonction():
    while True: # boucle tant que vrai
        compteur1()
# division de notre  fenetre en cadre
Label(fenetre,text="SELECTION DE LA FREQUENCE").pack()
grand_cadre = Frame(fenetre, borderwidth=1, relief=RAISED)
grand_cadre.pack(side=TOP, padx=20, pady=20)

Label(fenetre,text="BAUD RATE AND PORT").pack()

grand_cadre2 = Frame(fenetre, borderwidth=1, relief=RAISED)
grand_cadre2.pack(side=TOP, padx=20, pady=20)

grand_cadre3 = Frame(fenetre, borderwidth=1, relief=RAISED)
grand_cadre3.pack(side=TOP, padx=20, pady=20)

# sous cadre
cadre = Frame(grand_cadre, borderwidth=1, relief=SUNKEN)
cadre.pack(side=LEFT, padx=10, pady=10)

cadre2 = Frame(grand_cadre, borderwidth=1, relief=GROOVE)
cadre2.pack(side=BOTTOM, padx=10, pady=10)

cadre3 = Frame(grand_cadre, borderwidth=1, relief=SUNKEN)
cadre3.pack(side=LEFT, padx=10, pady=10)

cadre4 = Frame(grand_cadre2, borderwidth=1, relief=SUNKEN)
cadre4.pack(side=LEFT, padx=10, pady=10)

cadre5 = Frame(grand_cadre2, borderwidth=1, relief=GROOVE)
cadre5.pack(side=LEFT, padx=10, pady=10)

cadre6 = Frame(grand_cadre2, borderwidth=1, relief=SUNKEN)
cadre6.pack(side=BOTTOM, padx=10, pady=10)
# Ajouter des composants



valeur = DoubleVar()
scale1 = Scale(cadre,variable=valeur,orient='horizontal',from_=0,
        to=100,resolution=1,tickinterval=10,length=600,label='FREQUENCE MOTEUR').pack()
Label(cadre2,textvariable=valeur,font=("Arial",14,"bold",),bg="grey").pack(padx=10,pady=10)

heure = StringVar()
Label(grand_cadre,textvariable=heure,font=("Arial",14,"bold",),bg="grey").pack(padx=10,pady=10)
cycle_1s()

# ligne 2
Button(cadre4,text="send data",fg="white",bg="#001FFF",command=fonction,cursor="pirate").pack()
liste_com=["COM1","COM2","COM3","COM4","COM5","COM6","COM7","COM8",
           "COM9","COM10","COM11","COM12","COM13","COM14","COM15"]
liste_baud=["9600","115200"]
my_com = Spinbox(cadre5,values=liste_com,font=("Helvetica",10))
my_com.pack(pady=10)
my_baud = Spinbox(cadre6,values=liste_baud,font=("Helvetica",10))
my_baud.pack(pady=10)


fermeture = Button(fenetre,text="Quitter",fg="white",bg="#001FFF",command=fenetre.quit,cursor="pirate").pack()
fenetre.mainloop()