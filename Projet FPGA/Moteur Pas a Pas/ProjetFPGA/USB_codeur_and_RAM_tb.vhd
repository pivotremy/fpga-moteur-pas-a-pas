--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:04:27 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/USB_codeur_and_RAM_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: USB_codeur_and_RAM
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY USB_codeur_and_RAM_tb IS
END USB_codeur_and_RAM_tb;
 
ARCHITECTURE behavior OF USB_codeur_and_RAM_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT USB_codeur_and_RAM
    PORT(
         reset : IN  std_logic;
         codeur_in : IN  std_logic_vector(7 downto 0);
         enable : IN  std_logic;
         RX_DV : IN  std_logic;
         Rx_data_Uart : IN  std_logic_vector(7 downto 0);
         select_codeur_or_uart : IN  std_logic;
         start_motor_UP : OUT  std_logic;
         start_motor_down : OUT  std_logic;
         reglage_frequency_motor : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal codeur_in : std_logic_vector(7 downto 0) := (others => '0');
   signal enable : std_logic := '0';
   signal RX_DV : std_logic := '0';
   signal Rx_data_Uart : std_logic_vector(7 downto 0) := (others => '0');
   signal select_codeur_or_uart : std_logic := '0';

 	--Outputs
   signal start_motor_UP : std_logic;
   signal start_motor_down : std_logic;
   signal reglage_frequency_motor : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: USB_codeur_and_RAM PORT MAP (
          reset => reset,
          codeur_in => codeur_in,
          enable => enable,
          RX_DV => RX_DV,
          Rx_data_Uart => Rx_data_Uart,
          select_codeur_or_uart => select_codeur_or_uart,
          start_motor_UP => start_motor_UP,
          start_motor_down => start_motor_down,
          reglage_frequency_motor => reglage_frequency_motor
        );

   -- Clock process definitions
  
END;
