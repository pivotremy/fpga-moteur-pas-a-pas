--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:48:44 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/DECODE_2_to_8_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DECODE_2_to_8
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY DECODE_2_to_8_tb IS
END DECODE_2_to_8_tb;
 
ARCHITECTURE behavior OF DECODE_2_to_8_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DECODE_2_to_8
    PORT(
         SEL : IN  std_logic_vector(2 downto 0);
         DP1 : OUT  std_logic;
         afficheur_0 : OUT  std_logic;
         afficheur_1 : OUT  std_logic;
         afficheur_2 : OUT  std_logic;
         afficheur_3 : OUT  std_logic;
         afficheur_4 : OUT  std_logic;
         afficheur_5 : OUT  std_logic;
         afficheur_6 : OUT  std_logic;
         afficheur_7 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal SEL : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal DP1 : std_logic;
   signal afficheur_0 : std_logic;
   signal afficheur_1 : std_logic;
   signal afficheur_2 : std_logic;
   signal afficheur_3 : std_logic;
   signal afficheur_4 : std_logic;
   signal afficheur_5 : std_logic;
   signal afficheur_6 : std_logic;
   signal afficheur_7 : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DECODE_2_to_8 PORT MAP (
          SEL => SEL,
          DP1 => DP1,
          afficheur_0 => afficheur_0,
          afficheur_1 => afficheur_1,
          afficheur_2 => afficheur_2,
          afficheur_3 => afficheur_3,
          afficheur_4 => afficheur_4,
          afficheur_5 => afficheur_5,
          afficheur_6 => afficheur_6,
          afficheur_7 => afficheur_7
        );

   -- Clock process definitions 

   -- Stimulus process
  
END;
