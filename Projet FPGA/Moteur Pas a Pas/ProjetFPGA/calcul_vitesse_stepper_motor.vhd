library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;

entity calcul_vitesse_stepper_motor is
    Port ( frequency_in_motor : in  STD_LOGIC_VECTOR (7 downto 0);
           affichage_vitesse_digit0 : out  STD_LOGIC_VECTOR (3 downto 0);
           affichage_vitesse_digit1 : out  STD_LOGIC_VECTOR (3 downto 0);
           affichage_vitesse_digit2 : out  STD_LOGIC_VECTOR (3 downto 0);
           affichage_vitesse_digit3 : out  STD_LOGIC_VECTOR (3 downto 0));
end calcul_vitesse_stepper_motor;
architecture Behavioral of calcul_vitesse_stepper_motor is
signal Frequence: INTEGER range 0 to 255;
signal N: INTEGER; -- Tours/minutes ou *60 tr/heures
signal Q : STD_LOGIC_VECTOR (11 downto 0);
signal decalage_a_gauche: INTEGER;

begin

Frequence <= conv_integer(frequency_in_motor);
decalage_a_gauche <= (Frequence * 100000);
N <= (decalage_a_gauche / 4096) * 60;  --tours/s -->tours/minutes @100Hz 1.464 tr/min
Q <= std_logic_vector(to_unsigned(N/100, 12));

process(Q)
  -- variable temporaire 
  variable temp  : STD_LOGIC_VECTOR ( 11 downto 0 );  
  variable  bcd1  : STD_LOGIC_VECTOR ( 15  downto 0 );
  
begin 
	 bcd1   := (others => '0');
       temp (11 downto 0) :=  Q ;-- lire le signal Q dans la variable 
	 	 
	 for i in 0 to 11 loop
      

      if bcd1(3 downto 0) > 4 then 
        bcd1(3 downto 0) := bcd1(3 downto 0) + 3;
      end if;
      
      if bcd1(7 downto 4) > 4 then 
        bcd1(7 downto 4) := bcd1(7 downto 4) + 3;
      end if;
    
      if bcd1(11 downto 8) > 4 then  
        bcd1(11 downto 8) := bcd1(11 downto 8) + 3;
      end if;
    
      bcd1 := bcd1(14 downto 0) & temp(11);

      -- shift temp left by 1 bit
      temp := temp(10 downto 0) & '0';
    end loop; 
	 
	 	 	 -- set outputs    		 
          affichage_vitesse_digit0 <=  STD_LOGIC_VECTOR (bcd1(3 downto 0));
          affichage_vitesse_digit1 <=  STD_LOGIC_VECTOR (bcd1(7 downto 4));
          affichage_vitesse_digit2 <=  STD_LOGIC_VECTOR (bcd1(11 downto 8));
          affichage_vitesse_digit3 <=  STD_LOGIC_VECTOR (bcd1(15 downto 12));
       
end process;
end Behavioral;