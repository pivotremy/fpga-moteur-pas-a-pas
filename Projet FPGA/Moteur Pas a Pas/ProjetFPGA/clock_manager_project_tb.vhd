--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:43:40 03/02/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/clock_manager_project_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clock_manager_project
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY clock_manager_project_tb IS
END clock_manager_project_tb;
 
ARCHITECTURE behavior OF clock_manager_project_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT clock_manager_project
    PORT(
         clk : IN  std_logic;
         clk_div1 : OUT  std_logic;
         clk_div2 : OUT  std_logic;
         ce : IN  std_logic;
         reset : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal ce : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal clk_div1 : std_logic;
   signal clk_div2 : std_logic;

   -- Clock period definitions
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: clock_manager_project PORT MAP (
          clk => clk,
          clk_div1 => clk_div1,
          clk_div2 => clk_div2,
          ce => ce,
          reset => reset
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
   end process;
 
   ce_process :process
   begin
		ce <= '0';
		wait for 100 ns;
		ce <= '1';
		wait;
   end process;
	
	reset_process :process
   begin
		reset <= '1';
		wait for 100 ns;
		reset <= '0';
		wait;
   end process;
	
END;
