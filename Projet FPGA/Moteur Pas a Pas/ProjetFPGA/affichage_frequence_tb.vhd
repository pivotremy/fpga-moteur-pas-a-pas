--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:59:00 03/04/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/affichage_frequence_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: affichage_frequence
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY affichage_frequence_tb IS
END affichage_frequence_tb;
 
ARCHITECTURE behavior OF affichage_frequence_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT affichage_frequence
    PORT(
         frequency_in_motor : IN  std_logic_vector(7 downto 0);
         affichage_frequence_digit0 : OUT  std_logic_vector(3 downto 0);
         affichage_frequence_digit1 : OUT  std_logic_vector(3 downto 0);
         affichage_frequence_digit2 : OUT  std_logic_vector(3 downto 0);
         affichage_frequence_digit3 : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal frequency_in_motor : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal affichage_frequence_digit0 : std_logic_vector(3 downto 0);
   signal affichage_frequence_digit1 : std_logic_vector(3 downto 0);
   signal affichage_frequence_digit2 : std_logic_vector(3 downto 0);
   signal affichage_frequence_digit3 : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: affichage_frequence PORT MAP (
          frequency_in_motor => frequency_in_motor,
          affichage_frequence_digit0 => affichage_frequence_digit0,
          affichage_frequence_digit1 => affichage_frequence_digit1,
          affichage_frequence_digit2 => affichage_frequence_digit2,
          affichage_frequence_digit3 => affichage_frequence_digit3
        );

   -- Clock process definitions

 

   -- Stimulus process


END;
