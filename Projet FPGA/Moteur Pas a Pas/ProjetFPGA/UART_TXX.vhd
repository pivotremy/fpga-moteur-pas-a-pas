----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:38:37 03/11/2021 
-- Design Name: 
-- Module Name:    UART_TXX - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity UART_TXX is
  generic (
     CLKS_PER_BIT : integer := 10416     -- pour 100MHz horloge et 9600 bps
    );
  port (
    Clk       : in  std_logic;
    TX_DV     : in  std_logic;
    TX_Byte   : in  std_logic_vector(7 downto 0);
    TX_Active : out std_logic;
    TX_Serial : out std_logic;
    TX_Done   : out std_logic
    );
end UART_TXX;
 
 
architecture Behavioral of UART_TXX is
 
  type etat is (attente, Start_Bit, TX_Data_Bits,
                     Stop_Bit, s_Cleanup);

  signal state_machine: etat;
 
  signal Clk_Count : integer range 0 to CLKS_PER_BIT-1 := 0;
  signal r_Bit_Index : integer range 0 to 7 := 0;  -- 8 Bits Total
  signal r_TX_Data   : std_logic_vector(7 downto 0) := (others => '0');
  signal r_TX_Done   : std_logic := '0';
   
begin
    
  process(Clk)
  begin
    if rising_edge(Clk) then
         
      case state_machine is
 
        when attente =>
          TX_Active <= '0';
          TX_Serial <= '1';         -- Drive Line High for Idle
          r_TX_Done   <= '0';
          Clk_Count <= 0;
          r_Bit_Index <= 0;
 
          if TX_DV = '1' then
            r_TX_Data <= TX_Byte;
            state_machine <= Start_Bit;
          else
            state_machine <= attente;
          end if;
 
           
        -- Send out Start Bit. Start bit = 0
        when Start_Bit =>
          TX_Active <= '1';
          TX_Serial <= '0';
 
          -- Wait g_CLKS_PER_BIT-1 clock cycles for start bit to finish
          if Clk_Count < CLKS_PER_BIT-1 then
            Clk_Count <= Clk_Count + 1;
            state_machine   <= Start_Bit;
          else
            Clk_Count <= 0;
            state_machine   <= TX_Data_Bits;
          end if;
 
           
        -- Wait g_CLKS_PER_BIT-1 clock cycles for data bits to finish          
        when TX_Data_Bits =>
          TX_Serial <= r_TX_Data(r_Bit_Index);
           
          if Clk_Count < CLKS_PER_BIT-1 then
            Clk_Count <= Clk_Count + 1;
            state_machine  <= TX_Data_Bits;
          else
            Clk_Count <= 0;
             
            -- Check if we have sent out all bits
            if r_Bit_Index < 7 then
              r_Bit_Index <= r_Bit_Index + 1;
              state_machine <= TX_Data_Bits;
            else
              r_Bit_Index <= 0;
              state_machine <= Stop_Bit;
            end if;
          end if;
 
 
        -- Send out Stop bit.  Stop bit = 1
        when Stop_Bit =>
          TX_Serial <= '1';
 
          -- Wait g_CLKS_PER_BIT-1 clock cycles for Stop bit to finish
          if Clk_Count < CLKS_PER_BIT-1 then
            Clk_Count <= Clk_Count + 1;
            state_machine <= Stop_Bit;
          else
            r_TX_Done   <= '1';
            Clk_Count <= 0;
            state_machine <= s_Cleanup;
          end if;
 
                   
        -- Stay here 1 clock
        when s_Cleanup =>
          TX_Active <= '0';
          r_TX_Done   <= '1';
          state_machine <= attente;
           
             
        --when others =>
        --  state_machine <= s_Idle;
 
      end case;
    end if;
  end process;
 
  TX_Done <= r_TX_Done;
   
end Behavioral;