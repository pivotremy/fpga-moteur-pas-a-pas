--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:51:02 03/02/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/codeur_numerique_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: codeur_numerique
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY codeur_numerique_tb IS
END codeur_numerique_tb;
 
ARCHITECTURE behavior OF codeur_numerique_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT codeur_numerique
    PORT(
         CLK : IN  std_logic;
         reset : IN  std_logic;
         rotary_A : IN  std_logic;
         rotary_B : IN  std_logic;
         compte_out_codeur : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal reset : std_logic := '0';
   signal rotary_A : std_logic := '0';
   signal rotary_B : std_logic := '0';

 	--Outputs
   signal compte_out_codeur : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: codeur_numerique PORT MAP (
          CLK => CLK,
          reset => reset,
          rotary_A => rotary_A,
          rotary_B => rotary_B,
          compte_out_codeur => compte_out_codeur
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
