--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : TopLevel.vhf
-- /___/   /\     Timestamp : 03/11/2021 14:18:08
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family artix7 -flat -suppress -vhdl "C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/TopLevel.vhf" -w "C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/TopLevel.sch"
--Design Name: TopLevel
--Device: artix7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity TopLevel is
   port ( bouton_DOWN      : in    std_logic; 
          bouton_UP        : in    std_logic; 
          clk              : in    std_logic; 
          enable           : in    std_logic; 
          reset            : in    std_logic; 
          rotary_A         : in    std_logic; 
          rotary_B         : in    std_logic; 
          RX               : in    std_logic; 
          select_data_in   : in    std_logic; 
          afficheur_0      : out   std_logic; 
          afficheur_1      : out   std_logic; 
          afficheur_2      : out   std_logic; 
          afficheur_3      : out   std_logic; 
          afficheur_4      : out   std_logic; 
          afficheur_5      : out   std_logic; 
          afficheur_6      : out   std_logic; 
          afficheur_7      : out   std_logic; 
          afficheur_8      : out   std_logic; 
          afficheur_9      : out   std_logic; 
          afficheur_10     : out   std_logic; 
          afficheur_11     : out   std_logic; 
          Commande_4_Phase : out   std_logic_vector (3 downto 0); 
          DP               : out   std_logic; 
          EMPTY            : out   std_logic; 
          FULL             : out   std_logic; 
          SEG              : out   std_logic_vector (6 downto 0); 
          SEG2             : out   std_logic_vector (6 downto 0); 
          Visu_DOWN_freq   : out   std_logic; 
          Visu_UP_freq     : out   std_logic);
end TopLevel;

architecture BEHAVIORAL of TopLevel is
   attribute BOX_TYPE   : string ;
   signal XLXN_12          : std_logic;
   signal XLXN_17          : std_logic;
   signal XLXN_19          : std_logic_vector (2 downto 0);
   signal XLXN_23          : std_logic_vector (3 downto 0);
   signal XLXN_24          : std_logic_vector (3 downto 0);
   signal XLXN_25          : std_logic_vector (3 downto 0);
   signal XLXN_26          : std_logic_vector (3 downto 0);
   signal XLXN_27          : std_logic_vector (3 downto 0);
   signal XLXN_47          : std_logic_vector (3 downto 0);
   signal XLXN_48          : std_logic_vector (3 downto 0);
   signal XLXN_50          : std_logic_vector (3 downto 0);
   signal XLXN_51          : std_logic_vector (3 downto 0);
   signal XLXN_53          : std_logic_vector (3 downto 0);
   signal XLXN_212         : std_logic;
   signal XLXN_214         : std_logic_vector (7 downto 0);
   signal XLXN_215         : std_logic_vector (7 downto 0);
   signal XLXN_216         : std_logic;
   signal XLXN_222         : std_logic_vector (7 downto 0);
   signal XLXN_223         : std_logic;
   signal XLXN_224         : std_logic;
   signal XLXN_225         : std_logic;
   signal XLXN_228         : std_logic;
   signal XLXN_229         : std_logic;
   signal XLXN_231         : std_logic_vector (3 downto 0);
   signal XLXN_232         : std_logic_vector (3 downto 0);
   signal XLXN_233         : std_logic_vector (3 downto 0);
   signal XLXN_234         : std_logic_vector (3 downto 0);
   signal XLXN_235         : std_logic_vector (1 downto 0);
   signal XLXN_245         : std_logic_vector (7 downto 0);
   signal XLXN_248         : std_logic;
   component clock_manager_project
      port ( clk      : in    std_logic; 
             ce       : in    std_logic; 
             reset    : in    std_logic; 
             clk_div1 : out   std_logic; 
             clk_div2 : out   std_logic);
   end component;
   
   component compteur_3bits
      port ( clk             : in    std_logic; 
             reset           : in    std_logic; 
             enable          : in    std_logic; 
             sortie_compteur : out   std_logic_vector (2 downto 0));
   end component;
   
   component codeur_numerique
      port ( CLK               : in    std_logic; 
             reset             : in    std_logic; 
             rotary_A          : in    std_logic; 
             rotary_B          : in    std_logic; 
             compte_out_codeur : out   std_logic_vector (7 downto 0));
   end component;
   
   component state_machine_stepper_motor
      port ( clk                         : in    std_logic; 
             bouton_UP                   : in    std_logic; 
             bouton_DOWN                 : in    std_logic; 
             reset                       : in    std_logic; 
             change_frequency_motor      : in    std_logic_vector (7 downto 0); 
             visu_UP                     : out   std_logic; 
             visu_DOWN                   : out   std_logic; 
             Commandes_demi_pas          : out   std_logic_vector (3 downto 0); 
             vers_change_frequency_motor : out   std_logic_vector (7 downto 0));
   end component;
   
   component Gestion_commande_motor
      port ( bouton_montee              : in    std_logic; 
             bouton_descente            : in    std_logic; 
             start_motor_UP_PCcontrol   : in    std_logic; 
             start_motor_DOWN_PCcontrol : in    std_logic; 
             bouton_UP                  : out   std_logic; 
             bouton_DOWN                : out   std_logic);
   end component;
   
   component BCD7Segments
      port ( d : in    std_logic_vector (3 downto 0); 
             s : out   std_logic_vector (6 downto 0));
   end component;
   
   component CompteurBCD
      port ( CLK                    : in    std_logic; 
             bouton_UP              : in    std_logic; 
             bouton_DOWN            : in    std_logic; 
             Enable                 : in    std_logic; 
             Reset                  : in    std_logic; 
             change_frequency_motor : in    std_logic_vector (7 downto 0); 
             Full                   : out   std_logic; 
             Empty                  : out   std_logic; 
             BCD_U                  : out   std_logic_vector (3 downto 0); 
             BCD_D                  : out   std_logic_vector (3 downto 0); 
             BCD_H                  : out   std_logic_vector (3 downto 0); 
             BCD_T                  : out   std_logic_vector (3 downto 0));
   end component;
   
   component USB_codeur_and_RAM
      port ( reset                   : in    std_logic; 
             enable                  : in    std_logic; 
             RX_DV                   : in    std_logic; 
             select_codeur_or_uart   : in    std_logic; 
             codeur_in               : in    std_logic_vector (7 downto 0); 
             Rx_data_Uart            : in    std_logic_vector (7 downto 0); 
             start_motor_UP          : out   std_logic; 
             start_motor_down        : out   std_logic; 
             reglage_frequency_motor : out   std_logic_vector (7 downto 0));
   end component;
   
   component compteur_2bits
      port ( clk             : in    std_logic; 
             reset           : in    std_logic; 
             enable          : in    std_logic; 
             sortie_compteur : out   std_logic_vector (1 downto 0));
   end component;
   
   component MUX_4_to_1
      port ( A          : in    std_logic_vector (3 downto 0); 
             B          : in    std_logic_vector (3 downto 0); 
             C          : in    std_logic_vector (3 downto 0); 
             D          : in    std_logic_vector (3 downto 0); 
             SEL        : in    std_logic_vector (1 downto 0); 
             sortie_MUX : out   std_logic_vector (3 downto 0));
   end component;
   
   component UART_RX
      port ( clk       : in    std_logic; 
             RX_Serial : in    std_logic; 
             data_send : out   std_logic; 
             RX_Byte   : out   std_logic_vector (7 downto 0));
   end component;
   
   component BUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFG : component is "BLACK_BOX";
   
   component DECODE_2_to_8
      port ( SEL         : in    std_logic_vector (2 downto 0); 
             DP1         : out   std_logic; 
             afficheur_0 : out   std_logic; 
             afficheur_1 : out   std_logic; 
             afficheur_2 : out   std_logic; 
             afficheur_3 : out   std_logic; 
             afficheur_4 : out   std_logic; 
             afficheur_5 : out   std_logic; 
             afficheur_6 : out   std_logic; 
             afficheur_7 : out   std_logic);
   end component;
   
   component MUX_8_to_1
      port ( A          : in    std_logic_vector (3 downto 0); 
             B          : in    std_logic_vector (3 downto 0); 
             C          : in    std_logic_vector (3 downto 0); 
             D          : in    std_logic_vector (3 downto 0); 
             E          : in    std_logic_vector (3 downto 0); 
             F          : in    std_logic_vector (3 downto 0); 
             G          : in    std_logic_vector (3 downto 0); 
             H          : in    std_logic_vector (3 downto 0); 
             SEL        : in    std_logic_vector (2 downto 0); 
             sortie_mux : out   std_logic_vector (3 downto 0));
   end component;
   
   component affichage_frequence
      port ( frequency_in_motor         : in    std_logic_vector (7 downto 0); 
             affichage_frequence_digit0 : out   std_logic_vector (3 downto 0); 
             affichage_frequence_digit1 : out   std_logic_vector (3 downto 0); 
             affichage_frequence_digit2 : out   std_logic_vector (3 downto 0); 
             affichage_frequence_digit3 : out   std_logic_vector (3 downto 0));
   end component;
   
   component DECODE_2_to_4
      port ( SEL         : in    std_logic_vector (1 downto 0); 
             afficheur_0 : out   std_logic; 
             afficheur_1 : out   std_logic; 
             afficheur_2 : out   std_logic; 
             afficheur_3 : out   std_logic);
   end component;
   
   component calcul_vitesse_stepper_motor
      port ( frequency_in_motor       : in    std_logic_vector (7 downto 0); 
             affichage_vitesse_digit0 : out   std_logic_vector (3 downto 0); 
             affichage_vitesse_digit1 : out   std_logic_vector (3 downto 0); 
             affichage_vitesse_digit2 : out   std_logic_vector (3 downto 0); 
             affichage_vitesse_digit3 : out   std_logic_vector (3 downto 0));
   end component;
   
begin
   XLXI_1 : clock_manager_project
      port map (ce=>enable,
                clk=>clk,
                reset=>reset,
                clk_div1=>XLXN_12,
                clk_div2=>XLXN_17);
   
   XLXI_2 : compteur_3bits
      port map (clk=>XLXN_216,
                enable=>enable,
                reset=>reset,
                sortie_compteur(2 downto 0)=>XLXN_19(2 downto 0));
   
   XLXI_3 : codeur_numerique
      port map (CLK=>XLXN_223,
                reset=>reset,
                rotary_A=>rotary_A,
                rotary_B=>rotary_B,
                compte_out_codeur(7 downto 0)=>XLXN_222(7 downto 0));
   
   XLXI_4 : state_machine_stepper_motor
      port map (bouton_DOWN=>XLXN_229,
                bouton_UP=>XLXN_228,
                change_frequency_motor(7 downto 0)=>XLXN_215(7 downto 0),
                clk=>XLXN_223,
                reset=>reset,
                Commandes_demi_pas(3 downto 0)=>Commande_4_Phase(3 downto 0),
                vers_change_frequency_motor(7 downto 0)=>XLXN_245(7 downto 0),
                visu_DOWN=>Visu_DOWN_freq,
                visu_UP=>Visu_UP_freq);
   
   XLXI_5 : Gestion_commande_motor
      port map (bouton_descente=>bouton_DOWN,
                bouton_montee=>bouton_UP,
                start_motor_DOWN_PCcontrol=>XLXN_225,
                start_motor_UP_PCcontrol=>XLXN_224,
                bouton_DOWN=>XLXN_229,
                bouton_UP=>XLXN_228);
   
   XLXI_6 : BCD7Segments
      port map (d(3 downto 0)=>XLXN_27(3 downto 0),
                s(6 downto 0)=>SEG(6 downto 0));
   
   XLXI_7 : CompteurBCD
      port map (bouton_DOWN=>XLXN_229,
                bouton_UP=>XLXN_228,
                change_frequency_motor(7 downto 0)=>XLXN_215(7 downto 0),
                CLK=>XLXN_223,
                Enable=>enable,
                Reset=>reset,
                BCD_D(3 downto 0)=>XLXN_24(3 downto 0),
                BCD_H(3 downto 0)=>XLXN_25(3 downto 0),
                BCD_T(3 downto 0)=>XLXN_26(3 downto 0),
                BCD_U(3 downto 0)=>XLXN_23(3 downto 0),
                Empty=>EMPTY,
                Full=>FULL);
   
   XLXI_9 : USB_codeur_and_RAM
      port map (codeur_in(7 downto 0)=>XLXN_222(7 downto 0),
                enable=>enable,
                reset=>reset,
                Rx_data_Uart(7 downto 0)=>XLXN_214(7 downto 0),
                RX_DV=>XLXN_212,
                select_codeur_or_uart=>select_data_in,
                reglage_frequency_motor(7 downto 0)=>XLXN_215(7 downto 0),
                start_motor_down=>XLXN_225,
                start_motor_UP=>XLXN_224);
   
   XLXI_11 : compteur_2bits
      port map (clk=>XLXN_216,
                enable=>enable,
                reset=>reset,
                sortie_compteur(1 downto 0)=>XLXN_235(1 downto 0));
   
   XLXI_13 : MUX_4_to_1
      port map (A(3 downto 0)=>XLXN_231(3 downto 0),
                B(3 downto 0)=>XLXN_232(3 downto 0),
                C(3 downto 0)=>XLXN_233(3 downto 0),
                D(3 downto 0)=>XLXN_234(3 downto 0),
                SEL(1 downto 0)=>XLXN_235(1 downto 0),
                sortie_MUX(3 downto 0)=>XLXN_53(3 downto 0));
   
   XLXI_15 : UART_RX
      port map (clk=>XLXN_248,
                RX_Serial=>RX,
                data_send=>XLXN_212,
                RX_Byte(7 downto 0)=>XLXN_214(7 downto 0));
   
   XLXI_16 : BUFG
      port map (I=>XLXN_12,
                O=>XLXN_216);
   
   XLXI_17 : BUFG
      port map (I=>XLXN_17,
                O=>XLXN_223);
   
   XLXI_19 : DECODE_2_to_8
      port map (SEL(2 downto 0)=>XLXN_19(2 downto 0),
                afficheur_0=>afficheur_0,
                afficheur_1=>afficheur_1,
                afficheur_2=>afficheur_2,
                afficheur_3=>afficheur_3,
                afficheur_4=>afficheur_4,
                afficheur_5=>afficheur_5,
                afficheur_6=>afficheur_6,
                afficheur_7=>afficheur_7,
                DP1=>DP);
   
   XLXI_20 : MUX_8_to_1
      port map (A(3 downto 0)=>XLXN_23(3 downto 0),
                B(3 downto 0)=>XLXN_24(3 downto 0),
                C(3 downto 0)=>XLXN_25(3 downto 0),
                D(3 downto 0)=>XLXN_26(3 downto 0),
                E(3 downto 0)=>XLXN_47(3 downto 0),
                F(3 downto 0)=>XLXN_48(3 downto 0),
                G(3 downto 0)=>XLXN_50(3 downto 0),
                H(3 downto 0)=>XLXN_51(3 downto 0),
                SEL(2 downto 0)=>XLXN_19(2 downto 0),
                sortie_mux(3 downto 0)=>XLXN_27(3 downto 0));
   
   XLXI_22 : BCD7Segments
      port map (d(3 downto 0)=>XLXN_53(3 downto 0),
                s(6 downto 0)=>SEG2(6 downto 0));
   
   XLXI_23 : affichage_frequence
      port map (frequency_in_motor(7 downto 0)=>XLXN_245(7 downto 0),
                affichage_frequence_digit0(3 downto 0)=>XLXN_231(3 downto 0),
                affichage_frequence_digit1(3 downto 0)=>XLXN_232(3 downto 0),
                affichage_frequence_digit2(3 downto 0)=>XLXN_233(3 downto 0),
                affichage_frequence_digit3(3 downto 0)=>XLXN_234(3 downto 0));
   
   XLXI_29 : DECODE_2_to_4
      port map (SEL(1 downto 0)=>XLXN_235(1 downto 0),
                afficheur_0=>afficheur_8,
                afficheur_1=>afficheur_9,
                afficheur_2=>afficheur_10,
                afficheur_3=>afficheur_11);
   
   XLXI_30 : calcul_vitesse_stepper_motor
      port map (frequency_in_motor(7 downto 0)=>XLXN_245(7 downto 0),
                affichage_vitesse_digit0(3 downto 0)=>XLXN_47(3 downto 0),
                affichage_vitesse_digit1(3 downto 0)=>XLXN_48(3 downto 0),
                affichage_vitesse_digit2(3 downto 0)=>XLXN_50(3 downto 0),
                affichage_vitesse_digit3(3 downto 0)=>XLXN_51(3 downto 0));
   
   XLXI_32 : BUFG
      port map (I=>clk,
                O=>XLXN_248);
   
end BEHAVIORAL;


