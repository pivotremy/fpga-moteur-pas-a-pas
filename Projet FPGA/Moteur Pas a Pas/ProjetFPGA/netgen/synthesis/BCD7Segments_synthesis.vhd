--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: BCD7Segments_synthesis.vhd
-- /___/   /\     Timestamp: Wed Mar 03 10:42:28 2021
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm BCD7Segments -w -dir netgen/synthesis -ofmt vhdl -sim BCD7Segments.ngc BCD7Segments_synthesis.vhd 
-- Device	: xc7a100t-3-csg324
-- Input file	: BCD7Segments.ngc
-- Output file	: C:\Users\pivot\Desktop\Projet FPGA\Moteur Pas a Pas\ProjetFPGA\netgen\synthesis\BCD7Segments_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: BCD7Segments
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity BCD7Segments is
  port (
    d : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    s : out STD_LOGIC_VECTOR ( 6 downto 0 ) 
  );
end BCD7Segments;

architecture Structure of BCD7Segments is
  signal d_3_IBUF_0 : STD_LOGIC; 
  signal d_2_IBUF_1 : STD_LOGIC; 
  signal d_1_IBUF_2 : STD_LOGIC; 
  signal d_0_IBUF_3 : STD_LOGIC; 
  signal s_6_OBUF_4 : STD_LOGIC; 
  signal s_5_OBUF_5 : STD_LOGIC; 
  signal s_4_OBUF_6 : STD_LOGIC; 
  signal s_3_OBUF_7 : STD_LOGIC; 
  signal s_2_OBUF_8 : STD_LOGIC; 
  signal s_1_OBUF_9 : STD_LOGIC; 
  signal s_0_OBUF_10 : STD_LOGIC; 
begin
  Mram_s111 : LUT4
    generic map(
      INIT => X"E228"
    )
    port map (
      I0 => d_2_IBUF_1,
      I1 => d_0_IBUF_3,
      I2 => d_1_IBUF_2,
      I3 => d_3_IBUF_0,
      O => s_1_OBUF_9
    );
  Mram_s61 : LUT4
    generic map(
      INIT => X"0941"
    )
    port map (
      I0 => d_1_IBUF_2,
      I1 => d_2_IBUF_1,
      I2 => d_3_IBUF_0,
      I3 => d_0_IBUF_3,
      O => s_6_OBUF_4
    );
  Mram_s41 : LUT4
    generic map(
      INIT => X"02BA"
    )
    port map (
      I0 => d_0_IBUF_3,
      I1 => d_1_IBUF_2,
      I2 => d_2_IBUF_1,
      I3 => d_3_IBUF_0,
      O => s_4_OBUF_6
    );
  s_3_1 : LUT4
    generic map(
      INIT => X"A118"
    )
    port map (
      I0 => d_1_IBUF_2,
      I1 => d_3_IBUF_0,
      I2 => d_0_IBUF_3,
      I3 => d_2_IBUF_1,
      O => s_3_OBUF_7
    );
  s_0_1 : LUT4
    generic map(
      INIT => X"4190"
    )
    port map (
      I0 => d_1_IBUF_2,
      I1 => d_3_IBUF_0,
      I2 => d_0_IBUF_3,
      I3 => d_2_IBUF_1,
      O => s_0_OBUF_10
    );
  s_5_1 : LUT4
    generic map(
      INIT => X"2382"
    )
    port map (
      I0 => d_0_IBUF_3,
      I1 => d_3_IBUF_0,
      I2 => d_2_IBUF_1,
      I3 => d_1_IBUF_2,
      O => s_5_OBUF_5
    );
  s_2_1 : LUT4
    generic map(
      INIT => X"C140"
    )
    port map (
      I0 => d_0_IBUF_3,
      I1 => d_3_IBUF_0,
      I2 => d_2_IBUF_1,
      I3 => d_1_IBUF_2,
      O => s_2_OBUF_8
    );
  d_3_IBUF : IBUF
    port map (
      I => d(3),
      O => d_3_IBUF_0
    );
  d_2_IBUF : IBUF
    port map (
      I => d(2),
      O => d_2_IBUF_1
    );
  d_1_IBUF : IBUF
    port map (
      I => d(1),
      O => d_1_IBUF_2
    );
  d_0_IBUF : IBUF
    port map (
      I => d(0),
      O => d_0_IBUF_3
    );
  s_6_OBUF : OBUF
    port map (
      I => s_6_OBUF_4,
      O => s(6)
    );
  s_5_OBUF : OBUF
    port map (
      I => s_5_OBUF_5,
      O => s(5)
    );
  s_4_OBUF : OBUF
    port map (
      I => s_4_OBUF_6,
      O => s(4)
    );
  s_3_OBUF : OBUF
    port map (
      I => s_3_OBUF_7,
      O => s(3)
    );
  s_2_OBUF : OBUF
    port map (
      I => s_2_OBUF_8,
      O => s(2)
    );
  s_1_OBUF : OBUF
    port map (
      I => s_1_OBUF_9,
      O => s(1)
    );
  s_0_OBUF : OBUF
    port map (
      I => s_0_OBUF_10,
      O => s(0)
    );

end Structure;

