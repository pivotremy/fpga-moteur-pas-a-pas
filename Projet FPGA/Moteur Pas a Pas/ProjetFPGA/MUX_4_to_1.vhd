
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity MUX_4_to_1 is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           B : in  STD_LOGIC_VECTOR (3 downto 0);
           C : in  STD_LOGIC_VECTOR (3 downto 0);
           D : in  STD_LOGIC_VECTOR (3 downto 0);
           SEL : in  STD_LOGIC_VECTOR (1 downto 0);
           sortie_MUX : out  STD_LOGIC_VECTOR (3 downto 0));
end MUX_4_to_1;

architecture Behavioral of MUX_4_to_1 is

begin

process(sel,A,B,C,D)
begin
case  sel is
	  --when "000" =>  Sortie_Mux <= A;
	  when "01" =>  Sortie_Mux <= B;
	  when "10" =>  Sortie_Mux <= C;
	  when "11" =>  Sortie_Mux <= D;
	  when others => Sortie_Mux <= A;
end case;
end process;
end Behavioral;

