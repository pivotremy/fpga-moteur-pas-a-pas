<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_19(2:0)" />
        <signal name="XLXN_23(3:0)" />
        <signal name="XLXN_24(3:0)" />
        <signal name="XLXN_25(3:0)" />
        <signal name="XLXN_26(3:0)" />
        <signal name="XLXN_27(3:0)" />
        <signal name="XLXN_47(3:0)" />
        <signal name="XLXN_48(3:0)" />
        <signal name="XLXN_50(3:0)" />
        <signal name="XLXN_51(3:0)" />
        <signal name="XLXN_53(3:0)" />
        <signal name="SEG2(6:0)" />
        <signal name="DP" />
        <signal name="afficheur_0" />
        <signal name="Visu_UP_freq" />
        <signal name="Visu_DOWN_freq" />
        <signal name="Commande_4_Phase(3:0)" />
        <signal name="afficheur_1" />
        <signal name="afficheur_2" />
        <signal name="afficheur_3" />
        <signal name="afficheur_4" />
        <signal name="afficheur_5" />
        <signal name="afficheur_6" />
        <signal name="afficheur_7" />
        <signal name="afficheur_8" />
        <signal name="afficheur_9" />
        <signal name="afficheur_10" />
        <signal name="afficheur_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_17" />
        <signal name="bouton_UP" />
        <signal name="bouton_DOWN" />
        <signal name="rotary_A" />
        <signal name="rotary_B" />
        <signal name="RX" />
        <signal name="select_data_in" />
        <signal name="XLXN_212" />
        <signal name="XLXN_214(7:0)" />
        <signal name="XLXN_215(7:0)" />
        <signal name="XLXN_216" />
        <signal name="enable" />
        <signal name="reset" />
        <signal name="XLXN_222(7:0)" />
        <signal name="XLXN_223" />
        <signal name="XLXN_224" />
        <signal name="XLXN_225" />
        <signal name="XLXN_228" />
        <signal name="XLXN_229" />
        <signal name="XLXN_231(3:0)" />
        <signal name="XLXN_232(3:0)" />
        <signal name="XLXN_233(3:0)" />
        <signal name="XLXN_234(3:0)" />
        <signal name="XLXN_235(1:0)" />
        <signal name="FULL" />
        <signal name="EMPTY" />
        <signal name="XLXN_245(7:0)" />
        <signal name="SEG(6:0)" />
        <signal name="clk" />
        <signal name="XLXN_248" />
        <port polarity="Output" name="SEG2(6:0)" />
        <port polarity="Output" name="DP" />
        <port polarity="Output" name="afficheur_0" />
        <port polarity="Output" name="Visu_UP_freq" />
        <port polarity="Output" name="Visu_DOWN_freq" />
        <port polarity="Output" name="Commande_4_Phase(3:0)" />
        <port polarity="Output" name="afficheur_1" />
        <port polarity="Output" name="afficheur_2" />
        <port polarity="Output" name="afficheur_3" />
        <port polarity="Output" name="afficheur_4" />
        <port polarity="Output" name="afficheur_5" />
        <port polarity="Output" name="afficheur_6" />
        <port polarity="Output" name="afficheur_7" />
        <port polarity="Output" name="afficheur_8" />
        <port polarity="Output" name="afficheur_9" />
        <port polarity="Output" name="afficheur_10" />
        <port polarity="Output" name="afficheur_11" />
        <port polarity="Input" name="bouton_UP" />
        <port polarity="Input" name="bouton_DOWN" />
        <port polarity="Input" name="rotary_A" />
        <port polarity="Input" name="rotary_B" />
        <port polarity="Input" name="RX" />
        <port polarity="Input" name="select_data_in" />
        <port polarity="Input" name="enable" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="FULL" />
        <port polarity="Output" name="EMPTY" />
        <port polarity="Output" name="SEG(6:0)" />
        <port polarity="Input" name="clk" />
        <blockdef name="clock_manager_project">
            <timestamp>2021-3-3T10:22:35</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="compteur_3bits">
            <timestamp>2021-3-3T10:23:34</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
        </blockdef>
        <blockdef name="codeur_numerique">
            <timestamp>2021-3-3T10:22:52</timestamp>
            <rect width="352" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-236" height="24" />
            <line x2="480" y1="-224" y2="-224" x1="416" />
        </blockdef>
        <blockdef name="state_machine_stepper_motor">
            <timestamp>2021-3-3T10:23:51</timestamp>
            <rect width="656" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="784" y1="-288" y2="-288" x1="720" />
            <line x2="784" y1="-208" y2="-208" x1="720" />
            <rect width="64" x="720" y="-140" height="24" />
            <line x2="784" y1="-128" y2="-128" x1="720" />
            <rect width="64" x="720" y="-60" height="24" />
            <line x2="784" y1="-48" y2="-48" x1="720" />
        </blockdef>
        <blockdef name="Gestion_commande_motor">
            <timestamp>2021-3-6T16:16:15</timestamp>
            <rect width="480" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="608" y1="-224" y2="-224" x1="544" />
            <line x2="608" y1="-32" y2="-32" x1="544" />
        </blockdef>
        <blockdef name="BCD7Segments">
            <timestamp>2021-3-3T10:18:30</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="CompteurBCD">
            <timestamp>2021-3-3T10:15:44</timestamp>
            <rect width="448" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="576" y1="-352" y2="-352" x1="512" />
            <line x2="576" y1="-288" y2="-288" x1="512" />
            <rect width="64" x="512" y="-236" height="24" />
            <line x2="576" y1="-224" y2="-224" x1="512" />
            <rect width="64" x="512" y="-172" height="24" />
            <line x2="576" y1="-160" y2="-160" x1="512" />
            <rect width="64" x="512" y="-108" height="24" />
            <line x2="576" y1="-96" y2="-96" x1="512" />
            <rect width="64" x="512" y="-44" height="24" />
            <line x2="576" y1="-32" y2="-32" x1="512" />
        </blockdef>
        <blockdef name="USB_codeur_and_RAM">
            <timestamp>2021-3-3T10:21:0</timestamp>
            <rect width="544" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="672" y1="-352" y2="-352" x1="608" />
            <line x2="672" y1="-192" y2="-192" x1="608" />
            <rect width="64" x="608" y="-44" height="24" />
            <line x2="672" y1="-32" y2="-32" x1="608" />
        </blockdef>
        <blockdef name="compteur_2bits">
            <timestamp>2021-3-3T10:23:16</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
        </blockdef>
        <blockdef name="MUX_4_to_1">
            <timestamp>2021-3-3T10:19:30</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="MUX_8_to_1">
            <timestamp>2021-3-3T10:19:48</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <blockdef name="UART_RX">
            <timestamp>2021-3-3T10:20:37</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="bufg">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="0" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="DECODE_2_to_8">
            <timestamp>2021-3-3T10:18:50</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="affichage_frequence">
            <timestamp>2021-3-4T13:1:22</timestamp>
            <rect width="576" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="640" y="-236" height="24" />
            <line x2="704" y1="-224" y2="-224" x1="640" />
            <rect width="64" x="640" y="-172" height="24" />
            <line x2="704" y1="-160" y2="-160" x1="640" />
            <rect width="64" x="640" y="-108" height="24" />
            <line x2="704" y1="-96" y2="-96" x1="640" />
            <rect width="64" x="640" y="-44" height="24" />
            <line x2="704" y1="-32" y2="-32" x1="640" />
        </blockdef>
        <blockdef name="DECODE_2_to_4">
            <timestamp>2021-3-4T13:50:54</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="calcul_vitesse_stepper_motor">
            <timestamp>2021-3-11T10:46:12</timestamp>
            <rect width="544" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="608" y="-236" height="24" />
            <line x2="672" y1="-224" y2="-224" x1="608" />
            <rect width="64" x="608" y="-172" height="24" />
            <line x2="672" y1="-160" y2="-160" x1="608" />
            <rect width="64" x="608" y="-108" height="24" />
            <line x2="672" y1="-96" y2="-96" x1="608" />
            <rect width="64" x="608" y="-44" height="24" />
            <line x2="672" y1="-32" y2="-32" x1="608" />
        </blockdef>
        <block symbolname="compteur_3bits" name="XLXI_2">
            <blockpin signalname="XLXN_216" name="clk" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="enable" name="enable" />
            <blockpin signalname="XLXN_19(2:0)" name="sortie_compteur(2:0)" />
        </block>
        <block symbolname="codeur_numerique" name="XLXI_3">
            <blockpin signalname="XLXN_223" name="CLK" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="rotary_A" name="rotary_A" />
            <blockpin signalname="rotary_B" name="rotary_B" />
            <blockpin signalname="XLXN_222(7:0)" name="compte_out_codeur(7:0)" />
        </block>
        <block symbolname="state_machine_stepper_motor" name="XLXI_4">
            <blockpin signalname="XLXN_223" name="clk" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="XLXN_215(7:0)" name="change_frequency_motor(7:0)" />
            <blockpin signalname="Visu_UP_freq" name="visu_UP" />
            <blockpin signalname="Visu_DOWN_freq" name="visu_DOWN" />
            <blockpin signalname="Commande_4_Phase(3:0)" name="Commandes_demi_pas(3:0)" />
            <blockpin signalname="XLXN_245(7:0)" name="vers_change_frequency_motor(7:0)" />
        </block>
        <block symbolname="Gestion_commande_motor" name="XLXI_5">
            <blockpin signalname="bouton_UP" name="bouton_montee" />
            <blockpin signalname="bouton_DOWN" name="bouton_descente" />
            <blockpin signalname="XLXN_224" name="start_motor_UP_PCcontrol" />
            <blockpin signalname="XLXN_225" name="start_motor_DOWN_PCcontrol" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
        </block>
        <block symbolname="BCD7Segments" name="XLXI_6">
            <blockpin signalname="XLXN_27(3:0)" name="d(3:0)" />
            <blockpin signalname="SEG(6:0)" name="s(6:0)" />
        </block>
        <block symbolname="CompteurBCD" name="XLXI_7">
            <blockpin signalname="XLXN_223" name="CLK" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
            <blockpin signalname="enable" name="Enable" />
            <blockpin signalname="reset" name="Reset" />
            <blockpin signalname="XLXN_215(7:0)" name="change_frequency_motor(7:0)" />
            <blockpin signalname="FULL" name="Full" />
            <blockpin signalname="EMPTY" name="Empty" />
            <blockpin signalname="XLXN_23(3:0)" name="BCD_U(3:0)" />
            <blockpin signalname="XLXN_24(3:0)" name="BCD_D(3:0)" />
            <blockpin signalname="XLXN_25(3:0)" name="BCD_H(3:0)" />
            <blockpin signalname="XLXN_26(3:0)" name="BCD_T(3:0)" />
        </block>
        <block symbolname="USB_codeur_and_RAM" name="XLXI_9">
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="enable" name="enable" />
            <blockpin signalname="XLXN_212" name="RX_DV" />
            <blockpin signalname="select_data_in" name="select_codeur_or_uart" />
            <blockpin signalname="XLXN_222(7:0)" name="codeur_in(7:0)" />
            <blockpin signalname="XLXN_214(7:0)" name="Rx_data_Uart(7:0)" />
            <blockpin signalname="XLXN_224" name="start_motor_UP" />
            <blockpin signalname="XLXN_225" name="start_motor_down" />
            <blockpin signalname="XLXN_215(7:0)" name="reglage_frequency_motor(7:0)" />
        </block>
        <block symbolname="compteur_2bits" name="XLXI_11">
            <blockpin signalname="XLXN_216" name="clk" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="enable" name="enable" />
            <blockpin signalname="XLXN_235(1:0)" name="sortie_compteur(1:0)" />
        </block>
        <block symbolname="MUX_4_to_1" name="XLXI_13">
            <blockpin signalname="XLXN_231(3:0)" name="A(3:0)" />
            <blockpin signalname="XLXN_232(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_233(3:0)" name="C(3:0)" />
            <blockpin signalname="XLXN_234(3:0)" name="D(3:0)" />
            <blockpin signalname="XLXN_235(1:0)" name="SEL(1:0)" />
            <blockpin signalname="XLXN_53(3:0)" name="sortie_MUX(3:0)" />
        </block>
        <block symbolname="UART_RX" name="XLXI_15">
            <blockpin signalname="XLXN_248" name="clk" />
            <blockpin signalname="RX" name="RX_Serial" />
            <blockpin signalname="XLXN_212" name="data_send" />
            <blockpin signalname="XLXN_214(7:0)" name="RX_Byte(7:0)" />
        </block>
        <block symbolname="DECODE_2_to_8" name="XLXI_19">
            <blockpin signalname="XLXN_19(2:0)" name="SEL(2:0)" />
            <blockpin signalname="DP" name="DP1" />
            <blockpin signalname="afficheur_0" name="afficheur_0" />
            <blockpin signalname="afficheur_1" name="afficheur_1" />
            <blockpin signalname="afficheur_2" name="afficheur_2" />
            <blockpin signalname="afficheur_3" name="afficheur_3" />
            <blockpin signalname="afficheur_4" name="afficheur_4" />
            <blockpin signalname="afficheur_5" name="afficheur_5" />
            <blockpin signalname="afficheur_6" name="afficheur_6" />
            <blockpin signalname="afficheur_7" name="afficheur_7" />
        </block>
        <block symbolname="MUX_8_to_1" name="XLXI_20">
            <blockpin signalname="XLXN_23(3:0)" name="A(3:0)" />
            <blockpin signalname="XLXN_24(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_25(3:0)" name="C(3:0)" />
            <blockpin signalname="XLXN_26(3:0)" name="D(3:0)" />
            <blockpin signalname="XLXN_47(3:0)" name="E(3:0)" />
            <blockpin signalname="XLXN_48(3:0)" name="F(3:0)" />
            <blockpin signalname="XLXN_50(3:0)" name="G(3:0)" />
            <blockpin signalname="XLXN_51(3:0)" name="H(3:0)" />
            <blockpin signalname="XLXN_19(2:0)" name="SEL(2:0)" />
            <blockpin signalname="XLXN_27(3:0)" name="sortie_mux(3:0)" />
        </block>
        <block symbolname="BCD7Segments" name="XLXI_22">
            <blockpin signalname="XLXN_53(3:0)" name="d(3:0)" />
            <blockpin signalname="SEG2(6:0)" name="s(6:0)" />
        </block>
        <block symbolname="affichage_frequence" name="XLXI_23">
            <blockpin signalname="XLXN_245(7:0)" name="frequency_in_motor(7:0)" />
            <blockpin signalname="XLXN_231(3:0)" name="affichage_frequence_digit0(3:0)" />
            <blockpin signalname="XLXN_232(3:0)" name="affichage_frequence_digit1(3:0)" />
            <blockpin signalname="XLXN_233(3:0)" name="affichage_frequence_digit2(3:0)" />
            <blockpin signalname="XLXN_234(3:0)" name="affichage_frequence_digit3(3:0)" />
        </block>
        <block symbolname="clock_manager_project" name="XLXI_1">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="enable" name="ce" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="XLXN_12" name="clk_div1" />
            <blockpin signalname="XLXN_17" name="clk_div2" />
        </block>
        <block symbolname="bufg" name="XLXI_16">
            <blockpin signalname="XLXN_12" name="I" />
            <blockpin signalname="XLXN_216" name="O" />
        </block>
        <block symbolname="bufg" name="XLXI_17">
            <blockpin signalname="XLXN_17" name="I" />
            <blockpin signalname="XLXN_223" name="O" />
        </block>
        <block symbolname="DECODE_2_to_4" name="XLXI_29">
            <blockpin signalname="XLXN_235(1:0)" name="SEL(1:0)" />
            <blockpin signalname="afficheur_8" name="afficheur_0" />
            <blockpin signalname="afficheur_9" name="afficheur_1" />
            <blockpin signalname="afficheur_10" name="afficheur_2" />
            <blockpin signalname="afficheur_11" name="afficheur_3" />
        </block>
        <block symbolname="calcul_vitesse_stepper_motor" name="XLXI_30">
            <blockpin signalname="XLXN_245(7:0)" name="frequency_in_motor(7:0)" />
            <blockpin signalname="XLXN_47(3:0)" name="affichage_vitesse_digit0(3:0)" />
            <blockpin signalname="XLXN_48(3:0)" name="affichage_vitesse_digit1(3:0)" />
            <blockpin signalname="XLXN_50(3:0)" name="affichage_vitesse_digit2(3:0)" />
            <blockpin signalname="XLXN_51(3:0)" name="affichage_vitesse_digit3(3:0)" />
        </block>
        <block symbolname="bufg" name="XLXI_32">
            <blockpin signalname="clk" name="I" />
            <blockpin signalname="XLXN_248" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="816" y="1072" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_47(3:0)">
            <wire x2="3472" y1="1904" y2="1904" x1="2992" />
            <wire x2="3472" y1="1136" y2="1904" x1="3472" />
            <wire x2="4032" y1="1136" y2="1136" x1="3472" />
        </branch>
        <branch name="XLXN_48(3:0)">
            <wire x2="3456" y1="1968" y2="1968" x1="2992" />
            <wire x2="3456" y1="1200" y2="1968" x1="3456" />
            <wire x2="4032" y1="1200" y2="1200" x1="3456" />
        </branch>
        <branch name="XLXN_50(3:0)">
            <wire x2="3536" y1="2032" y2="2032" x1="2992" />
            <wire x2="3536" y1="1264" y2="2032" x1="3536" />
            <wire x2="4032" y1="1264" y2="1264" x1="3536" />
        </branch>
        <branch name="XLXN_51(3:0)">
            <wire x2="3552" y1="2096" y2="2096" x1="2992" />
            <wire x2="3552" y1="1328" y2="2096" x1="3552" />
            <wire x2="4032" y1="1328" y2="1328" x1="3552" />
        </branch>
        <branch name="XLXN_53(3:0)">
            <wire x2="4208" y1="1600" y2="1600" x1="4080" />
        </branch>
        <branch name="SEG2(6:0)">
            <wire x2="4688" y1="1600" y2="1600" x1="4592" />
        </branch>
        <branch name="DP">
            <wire x2="3616" y1="128" y2="128" x1="3600" />
            <wire x2="4000" y1="112" y2="112" x1="3616" />
            <wire x2="3616" y1="112" y2="128" x1="3616" />
        </branch>
        <branch name="afficheur_0">
            <wire x2="3616" y1="192" y2="192" x1="3600" />
            <wire x2="3968" y1="176" y2="176" x1="3616" />
            <wire x2="3616" y1="176" y2="192" x1="3616" />
        </branch>
        <branch name="Visu_DOWN_freq">
            <wire x2="3328" y1="752" y2="752" x1="2608" />
        </branch>
        <branch name="Commande_4_Phase(3:0)">
            <wire x2="3296" y1="832" y2="832" x1="2608" />
            <wire x2="3312" y1="816" y2="816" x1="3296" />
            <wire x2="3296" y1="816" y2="832" x1="3296" />
        </branch>
        <branch name="XLXN_19(2:0)">
            <wire x2="3120" y1="272" y2="272" x1="2736" />
            <wire x2="3200" y1="272" y2="272" x1="3120" />
            <wire x2="3120" y1="16" y2="272" x1="3120" />
            <wire x2="3712" y1="16" y2="16" x1="3120" />
            <wire x2="3712" y1="16" y2="1392" x1="3712" />
            <wire x2="4032" y1="1392" y2="1392" x1="3712" />
            <wire x2="3216" y1="128" y2="128" x1="3200" />
            <wire x2="3200" y1="128" y2="272" x1="3200" />
        </branch>
        <branch name="Visu_UP_freq">
            <wire x2="3072" y1="672" y2="672" x1="2608" />
            <wire x2="3072" y1="672" y2="704" x1="3072" />
            <wire x2="3088" y1="704" y2="704" x1="3072" />
        </branch>
        <branch name="afficheur_1">
            <wire x2="3616" y1="256" y2="256" x1="3600" />
            <wire x2="3984" y1="240" y2="240" x1="3616" />
            <wire x2="3616" y1="240" y2="256" x1="3616" />
        </branch>
        <branch name="afficheur_2">
            <wire x2="3616" y1="320" y2="320" x1="3600" />
            <wire x2="4000" y1="304" y2="304" x1="3616" />
            <wire x2="3616" y1="304" y2="320" x1="3616" />
        </branch>
        <branch name="afficheur_3">
            <wire x2="4016" y1="384" y2="384" x1="3600" />
        </branch>
        <branch name="afficheur_4">
            <wire x2="4064" y1="448" y2="448" x1="3600" />
        </branch>
        <branch name="afficheur_5">
            <wire x2="3616" y1="512" y2="512" x1="3600" />
            <wire x2="4048" y1="496" y2="496" x1="3616" />
            <wire x2="3616" y1="496" y2="512" x1="3616" />
        </branch>
        <branch name="afficheur_6">
            <wire x2="3616" y1="576" y2="576" x1="3600" />
            <wire x2="4048" y1="560" y2="560" x1="3616" />
            <wire x2="3616" y1="560" y2="576" x1="3616" />
        </branch>
        <branch name="afficheur_7">
            <wire x2="3616" y1="640" y2="640" x1="3600" />
            <wire x2="4032" y1="608" y2="608" x1="3616" />
            <wire x2="3616" y1="608" y2="640" x1="3616" />
        </branch>
        <instance x="3216" y="672" name="XLXI_19" orien="R0">
        </instance>
        <iomarker fontsize="28" x="4000" y="112" name="DP" orien="R0" />
        <iomarker fontsize="28" x="3968" y="176" name="afficheur_0" orien="R0" />
        <iomarker fontsize="28" x="3984" y="240" name="afficheur_1" orien="R0" />
        <iomarker fontsize="28" x="4000" y="304" name="afficheur_2" orien="R0" />
        <iomarker fontsize="28" x="4016" y="384" name="afficheur_3" orien="R0" />
        <iomarker fontsize="28" x="4064" y="448" name="afficheur_4" orien="R0" />
        <iomarker fontsize="28" x="4048" y="496" name="afficheur_5" orien="R0" />
        <iomarker fontsize="28" x="4048" y="560" name="afficheur_6" orien="R0" />
        <iomarker fontsize="28" x="4032" y="608" name="afficheur_7" orien="R0" />
        <instance x="2304" y="432" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_26(3:0)">
            <wire x2="3520" y1="1696" y2="1696" x1="2960" />
            <wire x2="3520" y1="1072" y2="1696" x1="3520" />
            <wire x2="4032" y1="1072" y2="1072" x1="3520" />
        </branch>
        <branch name="XLXN_25(3:0)">
            <wire x2="3504" y1="1632" y2="1632" x1="2960" />
            <wire x2="3504" y1="1008" y2="1632" x1="3504" />
            <wire x2="4032" y1="1008" y2="1008" x1="3504" />
        </branch>
        <branch name="XLXN_24(3:0)">
            <wire x2="3488" y1="1568" y2="1568" x1="2960" />
            <wire x2="3488" y1="944" y2="1568" x1="3488" />
            <wire x2="4032" y1="944" y2="944" x1="3488" />
        </branch>
        <branch name="XLXN_23(3:0)">
            <wire x2="3408" y1="1504" y2="1504" x1="2960" />
            <wire x2="3408" y1="880" y2="1504" x1="3408" />
            <wire x2="4032" y1="880" y2="880" x1="3408" />
        </branch>
        <branch name="XLXN_27(3:0)">
            <wire x2="4608" y1="880" y2="880" x1="4416" />
        </branch>
        <instance x="4208" y="1632" name="XLXI_22" orien="R0">
        </instance>
        <iomarker fontsize="28" x="4688" y="1600" name="SEG2(6:0)" orien="R0" />
        <instance x="3696" y="1888" name="XLXI_13" orien="R0">
        </instance>
        <branch name="afficheur_8">
            <wire x2="4304" y1="2064" y2="2064" x1="4096" />
        </branch>
        <branch name="afficheur_9">
            <wire x2="4288" y1="2128" y2="2128" x1="4096" />
        </branch>
        <branch name="afficheur_10">
            <wire x2="4288" y1="2192" y2="2192" x1="4096" />
        </branch>
        <branch name="afficheur_11">
            <wire x2="4288" y1="2256" y2="2256" x1="4096" />
        </branch>
        <iomarker fontsize="28" x="4288" y="2128" name="afficheur_9" orien="R0" />
        <iomarker fontsize="28" x="4288" y="2192" name="afficheur_10" orien="R0" />
        <iomarker fontsize="28" x="4288" y="2256" name="afficheur_11" orien="R0" />
        <instance x="4608" y="912" name="XLXI_6" orien="R0">
        </instance>
        <instance x="4032" y="1424" name="XLXI_20" orien="R0">
        </instance>
        <instance x="2256" y="2576" name="XLXI_23" orien="R0">
        </instance>
        <instance x="3280" y="2736" name="XLXI_11" orien="R0">
        </instance>
        <iomarker fontsize="28" x="3312" y="816" name="Commande_4_Phase(3:0)" orien="R0" />
        <iomarker fontsize="28" x="3328" y="752" name="Visu_DOWN_freq" orien="R0" />
        <iomarker fontsize="28" x="3088" y="704" name="Visu_UP_freq" orien="R0" />
        <instance x="1280" y="2192" name="XLXI_9" orien="R0">
        </instance>
        <instance x="864" y="464" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_12">
            <wire x2="1392" y1="304" y2="304" x1="1248" />
        </branch>
        <instance x="1392" y="336" name="XLXI_16" orien="R0" />
        <instance x="1392" y="448" name="XLXI_17" orien="R0" />
        <branch name="XLXN_17">
            <wire x2="1312" y1="432" y2="432" x1="1248" />
            <wire x2="1312" y1="416" y2="432" x1="1312" />
            <wire x2="1392" y1="416" y2="416" x1="1312" />
        </branch>
        <instance x="496" y="2208" name="XLXI_15" orien="R0">
        </instance>
        <instance x="896" y="1584" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1824" y="960" name="XLXI_4" orien="R0">
        </instance>
        <branch name="bouton_UP">
            <wire x2="816" y1="848" y2="848" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="848" name="bouton_UP" orien="R180" />
        <branch name="bouton_DOWN">
            <wire x2="816" y1="912" y2="912" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="912" name="bouton_DOWN" orien="R180" />
        <branch name="rotary_A">
            <wire x2="896" y1="1488" y2="1488" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="1488" name="rotary_A" orien="R180" />
        <branch name="rotary_B">
            <wire x2="896" y1="1552" y2="1552" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="1552" name="rotary_B" orien="R180" />
        <branch name="RX">
            <wire x2="496" y1="2176" y2="2176" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="2176" name="RX" orien="R180" />
        <branch name="select_data_in">
            <wire x2="1280" y1="2032" y2="2032" x1="1248" />
        </branch>
        <iomarker fontsize="28" x="1248" y="2032" name="select_data_in" orien="R180" />
        <branch name="XLXN_212">
            <wire x2="1056" y1="2112" y2="2112" x1="880" />
            <wire x2="1056" y1="1968" y2="2112" x1="1056" />
            <wire x2="1280" y1="1968" y2="1968" x1="1056" />
        </branch>
        <branch name="XLXN_214(7:0)">
            <wire x2="1072" y1="2176" y2="2176" x1="880" />
            <wire x2="1072" y1="2160" y2="2176" x1="1072" />
            <wire x2="1280" y1="2160" y2="2160" x1="1072" />
        </branch>
        <branch name="XLXN_215(7:0)">
            <wire x2="1824" y1="928" y2="928" x1="1744" />
            <wire x2="1744" y1="928" y2="1040" x1="1744" />
            <wire x2="2016" y1="1040" y2="1040" x1="1744" />
            <wire x2="2016" y1="1040" y2="2160" x1="2016" />
            <wire x2="2160" y1="2160" y2="2160" x1="2016" />
            <wire x2="2016" y1="2160" y2="2160" x1="1952" />
            <wire x2="2160" y1="1696" y2="2160" x1="2160" />
            <wire x2="2384" y1="1696" y2="1696" x1="2160" />
        </branch>
        <branch name="XLXN_216">
            <wire x2="1904" y1="304" y2="304" x1="1616" />
            <wire x2="1952" y1="304" y2="304" x1="1904" />
            <wire x2="1904" y1="304" y2="496" x1="1904" />
            <wire x2="3152" y1="496" y2="496" x1="1904" />
            <wire x2="3152" y1="496" y2="1376" x1="3152" />
            <wire x2="3248" y1="1376" y2="1376" x1="3152" />
            <wire x2="3248" y1="1376" y2="2576" x1="3248" />
            <wire x2="3280" y1="2576" y2="2576" x1="3248" />
            <wire x2="1952" y1="272" y2="304" x1="1952" />
            <wire x2="2304" y1="272" y2="272" x1="1952" />
        </branch>
        <branch name="enable">
            <wire x2="720" y1="368" y2="368" x1="592" />
            <wire x2="864" y1="368" y2="368" x1="720" />
            <wire x2="720" y1="368" y2="544" x1="720" />
            <wire x2="1520" y1="544" y2="544" x1="720" />
            <wire x2="1712" y1="544" y2="544" x1="1520" />
            <wire x2="1520" y1="544" y2="1568" x1="1520" />
            <wire x2="2112" y1="1568" y2="1568" x1="1520" />
            <wire x2="2384" y1="1568" y2="1568" x1="2112" />
            <wire x2="2112" y1="1568" y2="2704" x1="2112" />
            <wire x2="3280" y1="2704" y2="2704" x1="2112" />
            <wire x2="1216" y1="1728" y2="1904" x1="1216" />
            <wire x2="1280" y1="1904" y2="1904" x1="1216" />
            <wire x2="1456" y1="1728" y2="1728" x1="1216" />
            <wire x2="1456" y1="1568" y2="1728" x1="1456" />
            <wire x2="1520" y1="1568" y2="1568" x1="1456" />
            <wire x2="1712" y1="400" y2="544" x1="1712" />
            <wire x2="2304" y1="400" y2="400" x1="1712" />
        </branch>
        <branch name="reset">
            <wire x2="704" y1="1760" y2="1760" x1="208" />
            <wire x2="704" y1="1760" y2="1840" x1="704" />
            <wire x2="1280" y1="1840" y2="1840" x1="704" />
            <wire x2="208" y1="1760" y2="2640" x1="208" />
            <wire x2="3280" y1="2640" y2="2640" x1="208" />
            <wire x2="688" y1="432" y2="432" x1="592" />
            <wire x2="688" y1="432" y2="752" x1="688" />
            <wire x2="752" y1="432" y2="432" x1="688" />
            <wire x2="864" y1="432" y2="432" x1="752" />
            <wire x2="752" y1="432" y2="512" x1="752" />
            <wire x2="1632" y1="512" y2="512" x1="752" />
            <wire x2="592" y1="752" y2="1424" x1="592" />
            <wire x2="624" y1="1424" y2="1424" x1="592" />
            <wire x2="896" y1="1424" y2="1424" x1="624" />
            <wire x2="624" y1="1424" y2="1840" x1="624" />
            <wire x2="704" y1="1840" y2="1840" x1="624" />
            <wire x2="688" y1="752" y2="752" x1="592" />
            <wire x2="1680" y1="1216" y2="1216" x1="624" />
            <wire x2="1680" y1="1216" y2="1632" x1="1680" />
            <wire x2="2384" y1="1632" y2="1632" x1="1680" />
            <wire x2="624" y1="1216" y2="1424" x1="624" />
            <wire x2="1632" y1="336" y2="512" x1="1632" />
            <wire x2="2304" y1="336" y2="336" x1="1632" />
            <wire x2="1824" y1="864" y2="864" x1="1680" />
            <wire x2="1680" y1="864" y2="1216" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="592" y="368" name="enable" orien="R180" />
        <iomarker fontsize="28" x="592" y="432" name="reset" orien="R180" />
        <branch name="XLXN_222(7:0)">
            <wire x2="1280" y1="2096" y2="2096" x1="1216" />
            <wire x2="1216" y1="2096" y2="2272" x1="1216" />
            <wire x2="2000" y1="2272" y2="2272" x1="1216" />
            <wire x2="2000" y1="1360" y2="1360" x1="1376" />
            <wire x2="2000" y1="1360" y2="2272" x1="2000" />
        </branch>
        <branch name="XLXN_223">
            <wire x2="896" y1="1360" y2="1360" x1="880" />
            <wire x2="880" y1="1360" y2="1648" x1="880" />
            <wire x2="1696" y1="1648" y2="1648" x1="880" />
            <wire x2="1696" y1="416" y2="416" x1="1616" />
            <wire x2="1696" y1="416" y2="656" x1="1696" />
            <wire x2="1696" y1="656" y2="672" x1="1696" />
            <wire x2="1696" y1="672" y2="1648" x1="1696" />
            <wire x2="1824" y1="672" y2="672" x1="1696" />
            <wire x2="1760" y1="656" y2="656" x1="1696" />
            <wire x2="1760" y1="656" y2="1376" x1="1760" />
            <wire x2="2384" y1="1376" y2="1376" x1="1760" />
        </branch>
        <branch name="XLXN_224">
            <wire x2="816" y1="976" y2="976" x1="752" />
            <wire x2="752" y1="976" y2="1136" x1="752" />
            <wire x2="2032" y1="1136" y2="1136" x1="752" />
            <wire x2="2032" y1="1136" y2="1840" x1="2032" />
            <wire x2="2032" y1="1840" y2="1840" x1="1952" />
        </branch>
        <branch name="XLXN_225">
            <wire x2="816" y1="1040" y2="1040" x1="768" />
            <wire x2="768" y1="1040" y2="1120" x1="768" />
            <wire x2="1984" y1="1120" y2="1120" x1="768" />
            <wire x2="1984" y1="1120" y2="2000" x1="1984" />
            <wire x2="1984" y1="2000" y2="2000" x1="1952" />
        </branch>
        <branch name="XLXN_231(3:0)">
            <wire x2="3328" y1="2352" y2="2352" x1="2960" />
            <wire x2="3328" y1="1600" y2="2352" x1="3328" />
            <wire x2="3696" y1="1600" y2="1600" x1="3328" />
        </branch>
        <branch name="XLXN_232(3:0)">
            <wire x2="3312" y1="2416" y2="2416" x1="2960" />
            <wire x2="3312" y1="1664" y2="2416" x1="3312" />
            <wire x2="3696" y1="1664" y2="1664" x1="3312" />
        </branch>
        <branch name="XLXN_233(3:0)">
            <wire x2="3296" y1="2480" y2="2480" x1="2960" />
            <wire x2="3296" y1="1728" y2="2480" x1="3296" />
            <wire x2="3696" y1="1728" y2="1728" x1="3296" />
        </branch>
        <branch name="XLXN_234(3:0)">
            <wire x2="3264" y1="2544" y2="2544" x1="2960" />
            <wire x2="3264" y1="1792" y2="2544" x1="3264" />
            <wire x2="3696" y1="1792" y2="1792" x1="3264" />
        </branch>
        <branch name="XLXN_235(1:0)">
            <wire x2="3632" y1="1952" y2="1952" x1="3584" />
            <wire x2="3584" y1="1952" y2="2000" x1="3584" />
            <wire x2="3584" y1="2000" y2="2384" x1="3584" />
            <wire x2="3712" y1="2384" y2="2384" x1="3584" />
            <wire x2="3712" y1="2384" y2="2576" x1="3712" />
            <wire x2="3712" y1="2000" y2="2000" x1="3584" />
            <wire x2="3696" y1="1856" y2="1856" x1="3632" />
            <wire x2="3632" y1="1856" y2="1952" x1="3632" />
        </branch>
        <instance x="2384" y="1728" name="XLXI_7" orien="R0">
        </instance>
        <branch name="XLXN_228">
            <wire x2="1616" y1="848" y2="848" x1="1424" />
            <wire x2="1616" y1="736" y2="848" x1="1616" />
            <wire x2="1728" y1="736" y2="736" x1="1616" />
            <wire x2="1824" y1="736" y2="736" x1="1728" />
            <wire x2="1728" y1="736" y2="1440" x1="1728" />
            <wire x2="2384" y1="1440" y2="1440" x1="1728" />
        </branch>
        <branch name="XLXN_229">
            <wire x2="1632" y1="1040" y2="1040" x1="1424" />
            <wire x2="1632" y1="800" y2="1040" x1="1632" />
            <wire x2="1776" y1="800" y2="800" x1="1632" />
            <wire x2="1824" y1="800" y2="800" x1="1776" />
            <wire x2="1776" y1="800" y2="1504" x1="1776" />
            <wire x2="2384" y1="1504" y2="1504" x1="1776" />
        </branch>
        <branch name="FULL">
            <wire x2="2992" y1="1376" y2="1376" x1="2960" />
        </branch>
        <iomarker fontsize="28" x="2992" y="1376" name="FULL" orien="R0" />
        <branch name="EMPTY">
            <wire x2="2992" y1="1440" y2="1440" x1="2960" />
        </branch>
        <iomarker fontsize="28" x="2992" y="1440" name="EMPTY" orien="R0" />
        <branch name="XLXN_245(7:0)">
            <wire x2="2320" y1="2096" y2="2096" x1="2192" />
            <wire x2="2192" y1="2096" y2="2256" x1="2192" />
            <wire x2="2192" y1="2256" y2="2352" x1="2192" />
            <wire x2="2256" y1="2352" y2="2352" x1="2192" />
            <wire x2="3216" y1="2256" y2="2256" x1="2192" />
            <wire x2="3216" y1="912" y2="912" x1="2608" />
            <wire x2="3216" y1="912" y2="2256" x1="3216" />
        </branch>
        <branch name="SEG(6:0)">
            <wire x2="5024" y1="880" y2="880" x1="4992" />
        </branch>
        <iomarker fontsize="28" x="5024" y="880" name="SEG(6:0)" orien="R0" />
        <instance x="3712" y="2288" name="XLXI_29" orien="R0">
        </instance>
        <instance x="2320" y="2128" name="XLXI_30" orien="R0">
        </instance>
        <iomarker fontsize="28" x="4304" y="2064" name="afficheur_8" orien="R0" />
        <branch name="clk">
            <wire x2="48" y1="1536" y2="1632" x1="48" />
            <wire x2="96" y1="1632" y2="1632" x1="48" />
            <wire x2="112" y1="1632" y2="1632" x1="96" />
            <wire x2="112" y1="1632" y2="1648" x1="112" />
            <wire x2="336" y1="1536" y2="1536" x1="48" />
            <wire x2="112" y1="1648" y2="1648" x1="48" />
            <wire x2="48" y1="1648" y2="1696" x1="48" />
            <wire x2="112" y1="1696" y2="1696" x1="48" />
            <wire x2="336" y1="304" y2="304" x1="192" />
            <wire x2="864" y1="304" y2="304" x1="336" />
            <wire x2="336" y1="304" y2="1536" x1="336" />
        </branch>
        <branch name="XLXN_248">
            <wire x2="352" y1="1632" y2="1632" x1="336" />
            <wire x2="416" y1="1632" y2="1632" x1="352" />
            <wire x2="416" y1="1632" y2="2112" x1="416" />
            <wire x2="496" y1="2112" y2="2112" x1="416" />
            <wire x2="336" y1="1632" y2="1648" x1="336" />
            <wire x2="400" y1="1648" y2="1648" x1="336" />
            <wire x2="400" y1="1648" y2="1696" x1="400" />
            <wire x2="400" y1="1696" y2="1696" x1="336" />
        </branch>
        <iomarker fontsize="28" x="192" y="304" name="clk" orien="R180" />
        <instance x="112" y="1728" name="XLXI_32" orien="R0" />
    </sheet>
</drawing>