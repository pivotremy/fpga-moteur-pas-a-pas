--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:44:42 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/CompteurBCD_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: CompteurBCD
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY CompteurBCD_tb IS
END CompteurBCD_tb;
 
ARCHITECTURE behavior OF CompteurBCD_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CompteurBCD
    PORT(
         CLK : IN  std_logic;
         change_frequency_motor : IN  std_logic_vector(7 downto 0);
         bouton_UP : IN  std_logic;
         bouton_DOWN : IN  std_logic;
         Enable : IN  std_logic;
         Reset : IN  std_logic;
         Full : OUT  std_logic;
         Empty : OUT  std_logic;
         BCD_U : OUT  std_logic_vector(3 downto 0);
         BCD_D : OUT  std_logic_vector(3 downto 0);
         BCD_H : OUT  std_logic_vector(3 downto 0);
         BCD_T : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal change_frequency_motor : std_logic_vector(7 downto 0) := (others => '0');
   signal bouton_UP : std_logic := '0';
   signal bouton_DOWN : std_logic := '0';
   signal Enable : std_logic := '0';
   signal Reset : std_logic := '0';

 	--Outputs
   signal Full : std_logic;
   signal Empty : std_logic;
   signal BCD_U : std_logic_vector(3 downto 0);
   signal BCD_D : std_logic_vector(3 downto 0);
   signal BCD_H : std_logic_vector(3 downto 0);
   signal BCD_T : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CompteurBCD PORT MAP (
          CLK => CLK,
          change_frequency_motor => change_frequency_motor,
          bouton_UP => bouton_UP,
          bouton_DOWN => bouton_DOWN,
          Enable => Enable,
          Reset => Reset,
          Full => Full,
          Empty => Empty,
          BCD_U => BCD_U,
          BCD_D => BCD_D,
          BCD_H => BCD_H,
          BCD_T => BCD_T
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
