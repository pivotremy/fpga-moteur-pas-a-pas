--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:52:04 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/Gestion_commande_motor_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Gestion_commande_motor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Gestion_commande_motor_tb IS
END Gestion_commande_motor_tb;
 
ARCHITECTURE behavior OF Gestion_commande_motor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Gestion_commande_motor
    PORT(
         bouton_montee : IN  std_logic;
         bouton_descente : IN  std_logic;
         start_motor_UP_PCcontrol : IN  std_logic;
         start_motor_DOWN_PCcontrol : IN  std_logic;
         bouton_UP : OUT  std_logic;
         bouton_DOWN : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal bouton_montee : std_logic := '0';
   signal bouton_descente : std_logic := '0';
   signal start_motor_UP_PCcontrol : std_logic := '0';
   signal start_motor_DOWN_PCcontrol : std_logic := '0';

 	--Outputs
   signal bouton_UP : std_logic;
   signal bouton_DOWN : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Gestion_commande_motor PORT MAP (
          bouton_montee => bouton_montee,
          bouton_descente => bouton_descente,
          start_motor_UP_PCcontrol => start_motor_UP_PCcontrol,
          start_motor_DOWN_PCcontrol => start_motor_DOWN_PCcontrol,
          bouton_UP => bouton_UP,
          bouton_DOWN => bouton_DOWN
        );

   -- Clock process definitions
	  	bouton_descente_process :process
   begin
		bouton_descente<= '0';
		wait for 10 ns;
		bouton_descente<= '1';
		wait for 20 ns;
   end process;
	
	bouton_montee_process :process
   begin
		bouton_montee<= '0';
		wait for 10 ns;
		bouton_montee <= '1';
		wait for 20 ns;
   end process;
  

  
  	start_motor_UP_PCcontrol_process :process
   begin
		start_motor_UP_PCcontrol<= '0';
		wait for 30 ns;
		start_motor_UP_PCcontrol <= '1';
		wait for  50 ns;
   end process;
  
  	start_motor_DOWN_PCcontrol_process :process
   begin
		start_motor_DOWN_PCcontrol<= '0';
		wait for 50 ns;
		start_motor_DOWN_PCcontrol <= '1';
		wait for 70 ns;
   end process;
  

END;
