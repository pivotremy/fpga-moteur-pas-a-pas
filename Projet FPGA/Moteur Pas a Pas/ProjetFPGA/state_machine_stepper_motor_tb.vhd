--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:09:59 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/state_machine_stepper_motor_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: state_machine_stepper_motor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY state_machine_stepper_motor_tb IS
END state_machine_stepper_motor_tb;
 
ARCHITECTURE behavior OF state_machine_stepper_motor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT state_machine_stepper_motor
    PORT(
         clk : IN  std_logic;
         bouton_UP : IN  std_logic;
         bouton_DOWN : IN  std_logic;
         change_frequency_motor : IN  std_logic_vector(7 downto 0);
         Commandes_demi_pas : OUT  std_logic_vector(3 downto 0);
         vers_change_frequency_motor : OUT  std_logic_vector(7 downto 0);
         reset : IN  std_logic;
         visu_UP : OUT  std_logic;
         visu_DOWN : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal bouton_UP : std_logic := '0';
   signal bouton_DOWN : std_logic := '0';
   signal change_frequency_motor : std_logic_vector(7 downto 0) := (others => '0');
   signal reset : std_logic := '0';

 	--Outputs
   signal Commandes_demi_pas : std_logic_vector(3 downto 0);
   signal vers_change_frequency_motor : std_logic_vector(7 downto 0);
   signal visu_UP : std_logic;
   signal visu_DOWN : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: state_machine_stepper_motor PORT MAP (
          clk => clk,
          bouton_UP => bouton_UP,
          bouton_DOWN => bouton_DOWN,
          change_frequency_motor => change_frequency_motor,
          Commandes_demi_pas => Commandes_demi_pas,
          vers_change_frequency_motor => vers_change_frequency_motor,
          reset => reset,
          visu_UP => visu_UP,
          visu_DOWN => visu_DOWN
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
