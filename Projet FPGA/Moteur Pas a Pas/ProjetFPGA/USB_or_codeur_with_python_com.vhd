library IEEE;
use IEEE.STD_LOGIC_1164.all;
--use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;

entity USB_or_codeur_with_python_com is
    Port ( 
	        select_CW_or_CCW : STD_LOGIC_VECTOR (1 downto 0);
	        codeur_in : in  STD_LOGIC_VECTOR (7 downto 0);
           Rx_data_Uart : in  STD_LOGIC_VECTOR (7 downto 0);
           select_codeur_or_uart : in  STD_LOGIC;
           start_motor_UP : out  STD_LOGIC;
           start_motor_down : out  STD_LOGIC;
			  reglage_frequency_motor : out  STD_LOGIC_VECTOR (7 downto 0));
			  
end USB_or_codeur_with_python_com;

architecture Behavioral of USB_or_codeur_with_python_com is
signal UP : STD_LOGIC :='0';
signal DOWN : STD_LOGIC :='0';

begin

process(select_CW_or_CCW,UP,DOWN)
begin
case select_CW_or_CCW is
  when "00" => UP <='0'; DOWN <='0';
  when "01" => UP <='0'; DOWN <='1';
  when "10" => UP <='1'; DOWN <='0';
  when "11" => UP <='0'; DOWN <='0';
  when others => null;
end case;
end process;

reglage_frequency_motor <= Rx_data_Uart(7 downto 0) when select_codeur_or_uart ='1' else codeur_in;
start_motor_UP <= '1' when  UP = '1'  else '0'; 
start_motor_DOWN <= '1' when DOWN ='1' else '0'; 

end Behavioral;
