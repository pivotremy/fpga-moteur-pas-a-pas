-- D�claration des biblioth�ques utilis�es
library IEEE;
use ieee.std_logic_1164.all; -- inclus signaux am�lior�s du type std_ulogic--
use ieee.std_logic_arith.all; -- fournit le calcul num�rique--
use ieee.std_logic_unsigned.all; -- calcul num�rique non sign� sur le type std_logic_vector--

entity compteur_3bits is
    Port (
        clk             : in STD_LOGIC;
        reset           : in STD_LOGIC;
        enable          : in STD_LOGIC;
        sortie_compteur : out STD_LOGIC_VECTOR (2 downto 0)); -- -3 bits on affiche sur 8 digits--
end compteur_3bits;


architecture Behavioral of compteur_3bits is
    signal compte : std_logic_vector(2 downto 0); -- on declare un signal de comptage--
begin
--compteur 3 bits on compte au max jusqu'� 2^3 max--
    process(CLK,reset)
        BEGIN
            if reset ='1' then
                compte <= "000";
            elsif rising_edge(CLK) then
                if enable ='1' then
                    compte <= compte + 1;
                end if;
            end if;
    END PROCESS;
    Sortie_compteur <= compte;
end Behavioral;