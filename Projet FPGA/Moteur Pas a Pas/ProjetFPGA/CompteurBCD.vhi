
-- VHDL Instantiation Created from source file CompteurBCD.vhd -- 11:15:54 03/03/2021
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT CompteurBCD
	PORT(
		CLK : IN std_logic;
		change_frequency_motor : IN std_logic_vector(7 downto 0);
		bouton_UP : IN std_logic;
		bouton_DOWN : IN std_logic;
		Enable : IN std_logic;
		Reset : IN std_logic;          
		Full : OUT std_logic;
		Empty : OUT std_logic;
		BCD_U : OUT std_logic_vector(3 downto 0);
		BCD_D : OUT std_logic_vector(3 downto 0);
		BCD_H : OUT std_logic_vector(3 downto 0);
		BCD_T : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_CompteurBCD: CompteurBCD PORT MAP(
		CLK => ,
		change_frequency_motor => ,
		bouton_UP => ,
		bouton_DOWN => ,
		Enable => ,
		Reset => ,
		Full => ,
		Empty => ,
		BCD_U => ,
		BCD_D => ,
		BCD_H => ,
		BCD_T => 
	);


