--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:05:43 03/03/2021
-- Design Name:   
-- Module Name:   C:/Users/pivot/Desktop/Projet FPGA/Moteur Pas a Pas/ProjetFPGA/calcul_vitesse_stepper_motor_tb.vhd
-- Project Name:  ProjetFPGA
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: calcul_vitesse_stepper_motor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY calcul_vitesse_stepper_motor_tb IS
END calcul_vitesse_stepper_motor_tb;
 
ARCHITECTURE behavior OF calcul_vitesse_stepper_motor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT calcul_vitesse_stepper_motor
    PORT(
         frequency_in_motor : IN  std_logic_vector(7 downto 0);
         select_freq_or_vitesse : IN  std_logic;
         affichage_vitesse_digit0 : OUT  std_logic_vector(3 downto 0);
         affichage_vitesse_digit1 : OUT  std_logic_vector(3 downto 0);
         affichage_vitesse_digit2 : OUT  std_logic_vector(3 downto 0);
         affichage_vitesse_digit3 : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal frequency_in_motor : std_logic_vector(7 downto 0) := (others => '0');
   signal select_freq_or_vitesse : std_logic := '0';

 	--Outputs
   signal affichage_vitesse_digit0 : std_logic_vector(3 downto 0);
   signal affichage_vitesse_digit1 : std_logic_vector(3 downto 0);
   signal affichage_vitesse_digit2 : std_logic_vector(3 downto 0);
   signal affichage_vitesse_digit3 : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: calcul_vitesse_stepper_motor PORT MAP (
          frequency_in_motor => frequency_in_motor,
          select_freq_or_vitesse => select_freq_or_vitesse,
          affichage_vitesse_digit0 => affichage_vitesse_digit0,
          affichage_vitesse_digit1 => affichage_vitesse_digit1,
          affichage_vitesse_digit2 => affichage_vitesse_digit2,
          affichage_vitesse_digit3 => affichage_vitesse_digit3
        );

   -- Clock process definitions
  

END;
