
-- VHDL Instantiation Created from source file Gestion_commande_motor.vhd -- 17:16:20 03/06/2021
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Gestion_commande_motor
	PORT(
		bouton_montee : IN std_logic;
		bouton_descente : IN std_logic;
		start_motor_UP_PCcontrol : IN std_logic;
		start_motor_DOWN_PCcontrol : IN std_logic;          
		bouton_UP : OUT std_logic;
		bouton_DOWN : OUT std_logic
		);
	END COMPONENT;

	Inst_Gestion_commande_motor: Gestion_commande_motor PORT MAP(
		bouton_montee => ,
		bouton_descente => ,
		start_motor_UP_PCcontrol => ,
		start_motor_DOWN_PCcontrol => ,
		bouton_UP => ,
		bouton_DOWN => 
	);


