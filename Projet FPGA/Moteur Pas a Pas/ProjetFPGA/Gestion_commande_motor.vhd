library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Gestion_commande_motor is
Port (
bouton_montee : in STD_LOGIC;
bouton_descente : in STD_LOGIC;
start_motor_UP_PCcontrol : in STD_LOGIC;
start_motor_DOWN_PCcontrol : in STD_LOGIC;
bouton_UP : out STD_LOGIC;
bouton_DOWN : out STD_LOGIC
);
end Gestion_commande_motor;
architecture Behavioral of Gestion_commande_motor is
begin
process(bouton_montee,bouton_descente,start_motor_UP_PCcontrol,start_motor_DOWN_PCcontrol)
begin
	if  bouton_montee='1' or start_motor_UP_PCcontrol='1'  then
		bouton_UP <= '1';
	else
		bouton_UP <= '0';
		end if;
	if bouton_descente='1' or start_motor_DOWN_PCcontrol='1' then
		bouton_DOWN <= '1';
	else
		bouton_DOWN <= '0';
	end if;
end process;
end Behavioral;