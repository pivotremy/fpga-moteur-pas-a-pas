library IEEE;
use IEEE.STD_LOGIC_1164.all;
--use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;
entity USB_codeur_and_RAM is
Port (
reset : in std_logic;
codeur_in : in STD_LOGIC_VECTOR (7 downto 0);
enable: in std_logic;
RX_DV : in STD_LOGIC;
Rx_data_Uart : in STD_LOGIC_VECTOR (7 downto 0);
select_codeur_or_uart : in STD_LOGIC;
start_motor_UP : out STD_LOGIC;
start_motor_down : out STD_LOGIC;
reglage_frequency_motor : out STD_LOGIC_VECTOR (7 downto 0)
);
end USB_codeur_and_RAM;
architecture Behavioral of USB_codeur_and_RAM is
-- define the new type for the 3x8 octets RAM
type RAM_ARRAY is array (0 to 2) of std_logic_vector (7 downto 0);
signal RST_RAM : STD_LOGIC;
signal RAM_ADDR: STD_LOGIC_VECTOR (1 downto 0); -- Address to write/read RAM
-- initial values in the RAM
signal RAM: RAM_ARRAY :=(
x"00",x"00",x"00" );
signal bin_out : STD_LOGIC_VECTOR (10 downto 0);
signal bcd_in_0 : STD_LOGIC_VECTOR (3 downto 0);
signal bcd_in_10 : STD_LOGIC_VECTOR (3 downto 0);
signal bcd_in_100 : STD_LOGIC_VECTOR (3 downto 0);
signal UP : STD_LOGIC :='0';
signal DOWN : STD_LOGIC :='0';
begin
--Code Communication USB or codeur and RAM-
process(RX_DV,enable,reset,RST_RAM)
begin
if reset ='1' or RST_RAM ='1' then
RAM_ADDR <= "00";
RAM(0) <=X"00";
RAM(1) <=X"00";
RAM(2) <=X"00";
elsif rising_edge(RX_DV) then
if enable ='1' then
IF RAM_ADDR <=3 THEN
RAM_ADDR <= RAM_ADDR + 1;
END IF;
if Rx_data_uart>= X"30" and Rx_data_uart <= X"39" then
RAM(to_integer(unsigned(RAM_ADDR))) <= Rx_data_uart - X"30";
-- converts RAM_ADDR from std_logic_vector -> Unsigned -> Integer using numeric_std library
end if;
if Rx_data_uart< X"30" and Rx_data_uart> X"39" then
RAM(to_integer(unsigned(RAM_ADDR))) <= "00000000";
end if;
end if;
end if;
end process;
--this module is for converting a 4 digit BCD number into binary number.
--the range of the input in decimal is 0 to 999.
bcd_in_0 <= RAM(2)(3 downto 0); --use 4 bits in RAM 2
bcd_in_10 <= RAM(1)(3 downto 0); --use 4 bits in RAM 1
bcd_in_100 <= RAM(0)(3 downto 0); --use 4 bits in RAM 0
bin_out <= (bcd_in_0 * "01") --multiply by 1
+ (bcd_in_10 * "1010") --multiply by 10
+ (bcd_in_100 * "1100100"); --multiply by 100
RST_RAM <='1' when Rx_data_uart = X"72" else '0'; -- r du clavier
UP <='1' when Rx_data_uart = X"75" else '0'; -- u du clavier
DOWN <='1' when Rx_data_uart = X"64" else '0'; -- d du clavier
reglage_frequency_motor <= bin_out(7 downto 0) when select_codeur_or_uart ='1' else codeur_in;
start_motor_UP <= '1' when UP = '1' else '0';
start_motor_DOWN <= '1' when DOWN ='1' else '0';
end Behavioral;