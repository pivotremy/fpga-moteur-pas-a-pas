<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="artix7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_47(3:0)" />
        <signal name="XLXN_48(3:0)" />
        <signal name="XLXN_50(3:0)" />
        <signal name="XLXN_51(3:0)" />
        <signal name="XLXN_53(3:0)" />
        <signal name="SEG2(6:0)" />
        <signal name="XLXN_7" />
        <signal name="DP" />
        <signal name="XLXN_9" />
        <signal name="afficheur_0" />
        <signal name="Visu_DOWN_freq" />
        <signal name="Commande_4_Phase(3:0)" />
        <signal name="XLXN_13(3:0)" />
        <signal name="XLXN_19(2:0)" />
        <signal name="XLXN_15(2:0)" />
        <signal name="Visu_UP_freq" />
        <signal name="XLXN_17" />
        <signal name="afficheur_1" />
        <signal name="XLXN_19" />
        <signal name="afficheur_2" />
        <signal name="afficheur_3" />
        <signal name="afficheur_4" />
        <signal name="XLXN_23" />
        <signal name="afficheur_5" />
        <signal name="XLXN_25" />
        <signal name="afficheur_6" />
        <signal name="XLXN_27" />
        <signal name="afficheur_7" />
        <signal name="XLXN_26(3:0)" />
        <signal name="XLXN_25(3:0)" />
        <signal name="XLXN_24(3:0)" />
        <signal name="XLXN_23(3:0)" />
        <signal name="XLXN_27(3:0)" />
        <signal name="afficheur_8" />
        <signal name="afficheur_9" />
        <signal name="afficheur_10" />
        <signal name="afficheur_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_39" />
        <signal name="bouton_UP" />
        <signal name="bouton_DOWN" />
        <signal name="rotary_A" />
        <signal name="rotary_B" />
        <signal name="RX" />
        <signal name="select_data_in" />
        <signal name="XLXN_212" />
        <signal name="XLXN_215(7:0)" />
        <signal name="XLXN_216" />
        <signal name="XLXN_50" />
        <signal name="enable" />
        <signal name="XLXN_52" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="reset" />
        <signal name="XLXN_222(7:0)" />
        <signal name="XLXN_58(7:0)" />
        <signal name="XLXN_59" />
        <signal name="XLXN_223" />
        <signal name="XLXN_224" />
        <signal name="XLXN_225" />
        <signal name="XLXN_231(3:0)" />
        <signal name="XLXN_232(3:0)" />
        <signal name="XLXN_233(3:0)" />
        <signal name="XLXN_234(3:0)" />
        <signal name="XLXN_235(1:0)" />
        <signal name="XLXN_68(1:0)" />
        <signal name="XLXN_228" />
        <signal name="XLXN_229" />
        <signal name="FULL" />
        <signal name="EMPTY" />
        <signal name="XLXN_245(7:0)" />
        <signal name="XLXN_74(7:0)" />
        <signal name="SEG(6:0)" />
        <signal name="XLXN_76" />
        <signal name="clk" />
        <signal name="XLXN_248" />
        <signal name="XLXN_249" />
        <signal name="XLXN_250" />
        <signal name="XLXN_251(7:0)" />
        <signal name="XLXN_252" />
        <signal name="XLXN_253(7:0)" />
        <signal name="select_CW_or_CCW(1:0)" />
        <signal name="XLXN_261" />
        <signal name="XLXN_262" />
        <signal name="XLXN_263" />
        <signal name="XLXN_265" />
        <signal name="TX_active" />
        <signal name="TX" />
        <signal name="TX_clone" />
        <port polarity="Output" name="SEG2(6:0)" />
        <port polarity="Output" name="DP" />
        <port polarity="Output" name="afficheur_0" />
        <port polarity="Output" name="Visu_DOWN_freq" />
        <port polarity="Output" name="Commande_4_Phase(3:0)" />
        <port polarity="Output" name="Visu_UP_freq" />
        <port polarity="Output" name="afficheur_1" />
        <port polarity="Output" name="afficheur_2" />
        <port polarity="Output" name="afficheur_3" />
        <port polarity="Output" name="afficheur_4" />
        <port polarity="Output" name="afficheur_5" />
        <port polarity="Output" name="afficheur_6" />
        <port polarity="Output" name="afficheur_7" />
        <port polarity="Output" name="afficheur_8" />
        <port polarity="Output" name="afficheur_9" />
        <port polarity="Output" name="afficheur_10" />
        <port polarity="Output" name="afficheur_11" />
        <port polarity="Input" name="bouton_UP" />
        <port polarity="Input" name="bouton_DOWN" />
        <port polarity="Input" name="rotary_A" />
        <port polarity="Input" name="rotary_B" />
        <port polarity="Input" name="RX" />
        <port polarity="Input" name="select_data_in" />
        <port polarity="Input" name="enable" />
        <port polarity="Input" name="reset" />
        <port polarity="Output" name="FULL" />
        <port polarity="Output" name="EMPTY" />
        <port polarity="Output" name="SEG(6:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="select_CW_or_CCW(1:0)" />
        <port polarity="Output" name="TX_active" />
        <port polarity="Output" name="TX" />
        <port polarity="Output" name="TX_clone" />
        <blockdef name="Gestion_commande_motor">
            <timestamp>2021-3-6T16:16:15</timestamp>
            <rect width="480" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="608" y1="-224" y2="-224" x1="544" />
            <line x2="608" y1="-32" y2="-32" x1="544" />
        </blockdef>
        <blockdef name="DECODE_2_to_8">
            <timestamp>2021-3-3T10:18:50</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="compteur_3bits">
            <timestamp>2021-3-3T10:23:34</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
        </blockdef>
        <blockdef name="BCD7Segments">
            <timestamp>2021-3-3T10:18:30</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="MUX_4_to_1">
            <timestamp>2021-3-3T10:19:30</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="MUX_8_to_1">
            <timestamp>2021-3-3T10:19:48</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <blockdef name="affichage_frequence">
            <timestamp>2021-3-4T13:1:22</timestamp>
            <rect width="576" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="640" y="-236" height="24" />
            <line x2="704" y1="-224" y2="-224" x1="640" />
            <rect width="64" x="640" y="-172" height="24" />
            <line x2="704" y1="-160" y2="-160" x1="640" />
            <rect width="64" x="640" y="-108" height="24" />
            <line x2="704" y1="-96" y2="-96" x1="640" />
            <rect width="64" x="640" y="-44" height="24" />
            <line x2="704" y1="-32" y2="-32" x1="640" />
        </blockdef>
        <blockdef name="compteur_2bits">
            <timestamp>2021-3-3T10:23:16</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
        </blockdef>
        <blockdef name="clock_manager_project">
            <timestamp>2021-3-3T10:22:35</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="bufg">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="0" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="UART_RX">
            <timestamp>2021-3-3T10:20:37</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="codeur_numerique">
            <timestamp>2021-3-3T10:22:52</timestamp>
            <rect width="352" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-236" height="24" />
            <line x2="480" y1="-224" y2="-224" x1="416" />
        </blockdef>
        <blockdef name="state_machine_stepper_motor">
            <timestamp>2021-3-3T10:23:51</timestamp>
            <rect width="656" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="784" y1="-288" y2="-288" x1="720" />
            <line x2="784" y1="-208" y2="-208" x1="720" />
            <rect width="64" x="720" y="-140" height="24" />
            <line x2="784" y1="-128" y2="-128" x1="720" />
            <rect width="64" x="720" y="-60" height="24" />
            <line x2="784" y1="-48" y2="-48" x1="720" />
        </blockdef>
        <blockdef name="CompteurBCD">
            <timestamp>2021-3-3T10:15:44</timestamp>
            <rect width="448" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="576" y1="-352" y2="-352" x1="512" />
            <line x2="576" y1="-288" y2="-288" x1="512" />
            <rect width="64" x="512" y="-236" height="24" />
            <line x2="576" y1="-224" y2="-224" x1="512" />
            <rect width="64" x="512" y="-172" height="24" />
            <line x2="576" y1="-160" y2="-160" x1="512" />
            <rect width="64" x="512" y="-108" height="24" />
            <line x2="576" y1="-96" y2="-96" x1="512" />
            <rect width="64" x="512" y="-44" height="24" />
            <line x2="576" y1="-32" y2="-32" x1="512" />
        </blockdef>
        <blockdef name="DECODE_2_to_4">
            <timestamp>2021-3-4T13:50:54</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="calcul_vitesse_stepper_motor">
            <timestamp>2021-3-11T10:46:12</timestamp>
            <rect width="544" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="608" y="-236" height="24" />
            <line x2="672" y1="-224" y2="-224" x1="608" />
            <rect width="64" x="608" y="-172" height="24" />
            <line x2="672" y1="-160" y2="-160" x1="608" />
            <rect width="64" x="608" y="-108" height="24" />
            <line x2="672" y1="-96" y2="-96" x1="608" />
            <rect width="64" x="608" y="-44" height="24" />
            <line x2="672" y1="-32" y2="-32" x1="608" />
        </blockdef>
        <blockdef name="USB_or_codeur_with_python_com">
            <timestamp>2021-3-11T10:36:14</timestamp>
            <rect width="576" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="704" y1="-224" y2="-224" x1="640" />
            <line x2="704" y1="-128" y2="-128" x1="640" />
            <rect width="64" x="640" y="-44" height="24" />
            <line x2="704" y1="-32" y2="-32" x1="640" />
        </blockdef>
        <blockdef name="UART_TXX">
            <timestamp>2021-3-11T14:40:8</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="Gestion_commande_motor" name="XLXI_1">
            <blockpin signalname="bouton_UP" name="bouton_montee" />
            <blockpin signalname="bouton_DOWN" name="bouton_descente" />
            <blockpin signalname="XLXN_249" name="start_motor_UP_PCcontrol" />
            <blockpin signalname="XLXN_250" name="start_motor_DOWN_PCcontrol" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
        </block>
        <block symbolname="DECODE_2_to_8" name="XLXI_2">
            <blockpin signalname="XLXN_19(2:0)" name="SEL(2:0)" />
            <blockpin signalname="DP" name="DP1" />
            <blockpin signalname="afficheur_0" name="afficheur_0" />
            <blockpin signalname="afficheur_1" name="afficheur_1" />
            <blockpin signalname="afficheur_2" name="afficheur_2" />
            <blockpin signalname="afficheur_3" name="afficheur_3" />
            <blockpin signalname="afficheur_4" name="afficheur_4" />
            <blockpin signalname="afficheur_5" name="afficheur_5" />
            <blockpin signalname="afficheur_6" name="afficheur_6" />
            <blockpin signalname="afficheur_7" name="afficheur_7" />
        </block>
        <block symbolname="compteur_3bits" name="XLXI_3">
            <blockpin signalname="XLXN_216" name="clk" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="enable" name="enable" />
            <blockpin signalname="XLXN_19(2:0)" name="sortie_compteur(2:0)" />
        </block>
        <block symbolname="BCD7Segments" name="XLXI_22">
            <blockpin signalname="XLXN_53(3:0)" name="d(3:0)" />
            <blockpin signalname="SEG2(6:0)" name="s(6:0)" />
        </block>
        <block symbolname="MUX_4_to_1" name="XLXI_5">
            <blockpin signalname="XLXN_231(3:0)" name="A(3:0)" />
            <blockpin signalname="XLXN_232(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_233(3:0)" name="C(3:0)" />
            <blockpin signalname="XLXN_234(3:0)" name="D(3:0)" />
            <blockpin signalname="XLXN_235(1:0)" name="SEL(1:0)" />
            <blockpin signalname="XLXN_53(3:0)" name="sortie_MUX(3:0)" />
        </block>
        <block symbolname="BCD7Segments" name="XLXI_6">
            <blockpin signalname="XLXN_27(3:0)" name="d(3:0)" />
            <blockpin signalname="SEG(6:0)" name="s(6:0)" />
        </block>
        <block symbolname="MUX_8_to_1" name="XLXI_7">
            <blockpin signalname="XLXN_23(3:0)" name="A(3:0)" />
            <blockpin signalname="XLXN_24(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_25(3:0)" name="C(3:0)" />
            <blockpin signalname="XLXN_26(3:0)" name="D(3:0)" />
            <blockpin signalname="XLXN_47(3:0)" name="E(3:0)" />
            <blockpin signalname="XLXN_48(3:0)" name="F(3:0)" />
            <blockpin signalname="XLXN_50(3:0)" name="G(3:0)" />
            <blockpin signalname="XLXN_51(3:0)" name="H(3:0)" />
            <blockpin signalname="XLXN_19(2:0)" name="SEL(2:0)" />
            <blockpin signalname="XLXN_27(3:0)" name="sortie_mux(3:0)" />
        </block>
        <block symbolname="affichage_frequence" name="XLXI_23">
            <blockpin signalname="XLXN_245(7:0)" name="frequency_in_motor(7:0)" />
            <blockpin signalname="XLXN_231(3:0)" name="affichage_frequence_digit0(3:0)" />
            <blockpin signalname="XLXN_232(3:0)" name="affichage_frequence_digit1(3:0)" />
            <blockpin signalname="XLXN_233(3:0)" name="affichage_frequence_digit2(3:0)" />
            <blockpin signalname="XLXN_234(3:0)" name="affichage_frequence_digit3(3:0)" />
        </block>
        <block symbolname="compteur_2bits" name="XLXI_9">
            <blockpin signalname="XLXN_216" name="clk" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="enable" name="enable" />
            <blockpin signalname="XLXN_235(1:0)" name="sortie_compteur(1:0)" />
        </block>
        <block symbolname="clock_manager_project" name="XLXI_11">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="enable" name="ce" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="XLXN_12" name="clk_div1" />
            <blockpin signalname="XLXN_39" name="clk_div2" />
        </block>
        <block symbolname="bufg" name="XLXI_12">
            <blockpin signalname="XLXN_12" name="I" />
            <blockpin signalname="XLXN_216" name="O" />
        </block>
        <block symbolname="bufg" name="XLXI_13">
            <blockpin signalname="XLXN_39" name="I" />
            <blockpin signalname="XLXN_223" name="O" />
        </block>
        <block symbolname="UART_RX" name="XLXI_14">
            <blockpin signalname="XLXN_248" name="clk" />
            <blockpin signalname="RX" name="RX_Serial" />
            <blockpin signalname="XLXN_265" name="data_send" />
            <blockpin signalname="XLXN_251(7:0)" name="RX_Byte(7:0)" />
        </block>
        <block symbolname="codeur_numerique" name="XLXI_15">
            <blockpin signalname="XLXN_223" name="CLK" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="rotary_A" name="rotary_A" />
            <blockpin signalname="rotary_B" name="rotary_B" />
            <blockpin signalname="XLXN_222(7:0)" name="compte_out_codeur(7:0)" />
        </block>
        <block symbolname="state_machine_stepper_motor" name="XLXI_4">
            <blockpin signalname="XLXN_223" name="clk" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
            <blockpin signalname="reset" name="reset" />
            <blockpin signalname="XLXN_215(7:0)" name="change_frequency_motor(7:0)" />
            <blockpin signalname="Visu_UP_freq" name="visu_UP" />
            <blockpin signalname="Visu_DOWN_freq" name="visu_DOWN" />
            <blockpin signalname="Commande_4_Phase(3:0)" name="Commandes_demi_pas(3:0)" />
            <blockpin signalname="XLXN_245(7:0)" name="vers_change_frequency_motor(7:0)" />
        </block>
        <block symbolname="CompteurBCD" name="XLXI_17">
            <blockpin signalname="XLXN_223" name="CLK" />
            <blockpin signalname="XLXN_228" name="bouton_UP" />
            <blockpin signalname="XLXN_229" name="bouton_DOWN" />
            <blockpin signalname="enable" name="Enable" />
            <blockpin signalname="reset" name="Reset" />
            <blockpin signalname="XLXN_215(7:0)" name="change_frequency_motor(7:0)" />
            <blockpin signalname="FULL" name="Full" />
            <blockpin signalname="EMPTY" name="Empty" />
            <blockpin signalname="XLXN_23(3:0)" name="BCD_U(3:0)" />
            <blockpin signalname="XLXN_24(3:0)" name="BCD_D(3:0)" />
            <blockpin signalname="XLXN_25(3:0)" name="BCD_H(3:0)" />
            <blockpin signalname="XLXN_26(3:0)" name="BCD_T(3:0)" />
        </block>
        <block symbolname="DECODE_2_to_4" name="XLXI_29">
            <blockpin signalname="XLXN_235(1:0)" name="SEL(1:0)" />
            <blockpin signalname="afficheur_8" name="afficheur_0" />
            <blockpin signalname="afficheur_9" name="afficheur_1" />
            <blockpin signalname="afficheur_10" name="afficheur_2" />
            <blockpin signalname="afficheur_11" name="afficheur_3" />
        </block>
        <block symbolname="calcul_vitesse_stepper_motor" name="XLXI_30">
            <blockpin signalname="XLXN_245(7:0)" name="frequency_in_motor(7:0)" />
            <blockpin signalname="XLXN_47(3:0)" name="affichage_vitesse_digit0(3:0)" />
            <blockpin signalname="XLXN_48(3:0)" name="affichage_vitesse_digit1(3:0)" />
            <blockpin signalname="XLXN_50(3:0)" name="affichage_vitesse_digit2(3:0)" />
            <blockpin signalname="XLXN_51(3:0)" name="affichage_vitesse_digit3(3:0)" />
        </block>
        <block symbolname="bufg" name="XLXI_32">
            <blockpin signalname="clk" name="I" />
            <blockpin signalname="XLXN_248" name="O" />
        </block>
        <block symbolname="USB_or_codeur_with_python_com" name="XLXI_34">
            <blockpin signalname="select_data_in" name="select_codeur_or_uart" />
            <blockpin signalname="select_CW_or_CCW(1:0)" name="select_CW_or_CCW(1:0)" />
            <blockpin signalname="XLXN_222(7:0)" name="codeur_in(7:0)" />
            <blockpin signalname="XLXN_251(7:0)" name="Rx_data_Uart(7:0)" />
            <blockpin signalname="XLXN_249" name="start_motor_UP" />
            <blockpin signalname="XLXN_250" name="start_motor_down" />
            <blockpin signalname="XLXN_215(7:0)" name="reglage_frequency_motor(7:0)" />
        </block>
        <block symbolname="bufg" name="XLXI_39">
            <blockpin signalname="clk" name="I" />
            <blockpin signalname="XLXN_261" name="O" />
        </block>
        <block symbolname="UART_TXX" name="XLXI_44">
            <blockpin signalname="XLXN_261" name="Clk" />
            <blockpin signalname="XLXN_265" name="TX_DV" />
            <blockpin signalname="XLXN_251(7:0)" name="TX_Byte(7:0)" />
            <blockpin signalname="TX_active" name="TX_Active" />
            <blockpin signalname="TX" name="TX_Serial" />
            <blockpin signalname="TX_clone" name="TX_Done" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="944" y="1280" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_47(3:0)">
            <wire x2="3600" y1="2112" y2="2112" x1="3120" />
            <wire x2="3600" y1="1344" y2="2112" x1="3600" />
            <wire x2="4160" y1="1344" y2="1344" x1="3600" />
        </branch>
        <branch name="XLXN_48(3:0)">
            <wire x2="3584" y1="2176" y2="2176" x1="3120" />
            <wire x2="3584" y1="1408" y2="2176" x1="3584" />
            <wire x2="4160" y1="1408" y2="1408" x1="3584" />
        </branch>
        <branch name="XLXN_50(3:0)">
            <wire x2="3664" y1="2240" y2="2240" x1="3120" />
            <wire x2="3664" y1="1472" y2="2240" x1="3664" />
            <wire x2="4160" y1="1472" y2="1472" x1="3664" />
        </branch>
        <branch name="XLXN_51(3:0)">
            <wire x2="3680" y1="2304" y2="2304" x1="3120" />
            <wire x2="3680" y1="1536" y2="2304" x1="3680" />
            <wire x2="4160" y1="1536" y2="1536" x1="3680" />
        </branch>
        <branch name="XLXN_53(3:0)">
            <wire x2="4336" y1="1808" y2="1808" x1="4208" />
        </branch>
        <branch name="SEG2(6:0)">
            <wire x2="4816" y1="1808" y2="1808" x1="4720" />
        </branch>
        <branch name="DP">
            <wire x2="3744" y1="336" y2="336" x1="3728" />
            <wire x2="4128" y1="320" y2="320" x1="3744" />
            <wire x2="3744" y1="320" y2="336" x1="3744" />
        </branch>
        <branch name="afficheur_0">
            <wire x2="3744" y1="400" y2="400" x1="3728" />
            <wire x2="4096" y1="384" y2="384" x1="3744" />
            <wire x2="3744" y1="384" y2="400" x1="3744" />
        </branch>
        <branch name="Visu_DOWN_freq">
            <wire x2="3456" y1="960" y2="960" x1="2736" />
        </branch>
        <branch name="Commande_4_Phase(3:0)">
            <wire x2="3424" y1="1040" y2="1040" x1="2736" />
            <wire x2="3440" y1="1024" y2="1024" x1="3424" />
            <wire x2="3424" y1="1024" y2="1040" x1="3424" />
        </branch>
        <branch name="XLXN_19(2:0)">
            <wire x2="3248" y1="480" y2="480" x1="2864" />
            <wire x2="3328" y1="480" y2="480" x1="3248" />
            <wire x2="3248" y1="224" y2="480" x1="3248" />
            <wire x2="3840" y1="224" y2="224" x1="3248" />
            <wire x2="3840" y1="224" y2="1600" x1="3840" />
            <wire x2="4160" y1="1600" y2="1600" x1="3840" />
            <wire x2="3344" y1="336" y2="336" x1="3328" />
            <wire x2="3328" y1="336" y2="480" x1="3328" />
        </branch>
        <branch name="Visu_UP_freq">
            <wire x2="3200" y1="880" y2="880" x1="2736" />
            <wire x2="3200" y1="880" y2="912" x1="3200" />
            <wire x2="3216" y1="912" y2="912" x1="3200" />
        </branch>
        <branch name="afficheur_1">
            <wire x2="3744" y1="464" y2="464" x1="3728" />
            <wire x2="4112" y1="448" y2="448" x1="3744" />
            <wire x2="3744" y1="448" y2="464" x1="3744" />
        </branch>
        <branch name="afficheur_2">
            <wire x2="3744" y1="528" y2="528" x1="3728" />
            <wire x2="4128" y1="512" y2="512" x1="3744" />
            <wire x2="3744" y1="512" y2="528" x1="3744" />
        </branch>
        <branch name="afficheur_3">
            <wire x2="4144" y1="592" y2="592" x1="3728" />
        </branch>
        <branch name="afficheur_4">
            <wire x2="4192" y1="656" y2="656" x1="3728" />
        </branch>
        <branch name="afficheur_5">
            <wire x2="3744" y1="720" y2="720" x1="3728" />
            <wire x2="4176" y1="704" y2="704" x1="3744" />
            <wire x2="3744" y1="704" y2="720" x1="3744" />
        </branch>
        <branch name="afficheur_6">
            <wire x2="3744" y1="784" y2="784" x1="3728" />
            <wire x2="4176" y1="768" y2="768" x1="3744" />
            <wire x2="3744" y1="768" y2="784" x1="3744" />
        </branch>
        <branch name="afficheur_7">
            <wire x2="3744" y1="848" y2="848" x1="3728" />
            <wire x2="4160" y1="816" y2="816" x1="3744" />
            <wire x2="3744" y1="816" y2="848" x1="3744" />
        </branch>
        <instance x="3344" y="880" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2432" y="640" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_26(3:0)">
            <wire x2="3648" y1="1904" y2="1904" x1="3088" />
            <wire x2="3648" y1="1280" y2="1904" x1="3648" />
            <wire x2="4160" y1="1280" y2="1280" x1="3648" />
        </branch>
        <branch name="XLXN_25(3:0)">
            <wire x2="3632" y1="1840" y2="1840" x1="3088" />
            <wire x2="3632" y1="1216" y2="1840" x1="3632" />
            <wire x2="4160" y1="1216" y2="1216" x1="3632" />
        </branch>
        <branch name="XLXN_24(3:0)">
            <wire x2="3616" y1="1776" y2="1776" x1="3088" />
            <wire x2="3616" y1="1152" y2="1776" x1="3616" />
            <wire x2="4160" y1="1152" y2="1152" x1="3616" />
        </branch>
        <branch name="XLXN_23(3:0)">
            <wire x2="3536" y1="1712" y2="1712" x1="3088" />
            <wire x2="3536" y1="1088" y2="1712" x1="3536" />
            <wire x2="4160" y1="1088" y2="1088" x1="3536" />
        </branch>
        <branch name="XLXN_27(3:0)">
            <wire x2="4736" y1="1088" y2="1088" x1="4544" />
        </branch>
        <instance x="4336" y="1840" name="XLXI_22" orien="R0">
        </instance>
        <instance x="3824" y="2096" name="XLXI_5" orien="R0">
        </instance>
        <branch name="afficheur_8">
            <wire x2="4432" y1="2272" y2="2272" x1="4224" />
        </branch>
        <branch name="afficheur_9">
            <wire x2="4416" y1="2336" y2="2336" x1="4224" />
        </branch>
        <branch name="afficheur_10">
            <wire x2="4416" y1="2400" y2="2400" x1="4224" />
        </branch>
        <branch name="afficheur_11">
            <wire x2="4416" y1="2464" y2="2464" x1="4224" />
        </branch>
        <instance x="4736" y="1120" name="XLXI_6" orien="R0">
        </instance>
        <instance x="4160" y="1632" name="XLXI_7" orien="R0">
        </instance>
        <instance x="2384" y="2784" name="XLXI_23" orien="R0">
        </instance>
        <instance x="3408" y="2944" name="XLXI_9" orien="R0">
        </instance>
        <instance x="992" y="672" name="XLXI_11" orien="R0">
        </instance>
        <branch name="XLXN_12">
            <wire x2="1520" y1="512" y2="512" x1="1376" />
        </branch>
        <instance x="1520" y="544" name="XLXI_12" orien="R0" />
        <instance x="1520" y="656" name="XLXI_13" orien="R0" />
        <branch name="XLXN_39">
            <wire x2="1440" y1="640" y2="640" x1="1376" />
            <wire x2="1440" y1="624" y2="640" x1="1440" />
            <wire x2="1520" y1="624" y2="624" x1="1440" />
        </branch>
        <instance x="624" y="2416" name="XLXI_14" orien="R0">
        </instance>
        <instance x="1024" y="1792" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1952" y="1168" name="XLXI_4" orien="R0">
        </instance>
        <branch name="bouton_UP">
            <wire x2="944" y1="1056" y2="1056" x1="912" />
        </branch>
        <branch name="bouton_DOWN">
            <wire x2="944" y1="1120" y2="1120" x1="912" />
        </branch>
        <branch name="rotary_A">
            <wire x2="1024" y1="1696" y2="1696" x1="992" />
        </branch>
        <branch name="rotary_B">
            <wire x2="1024" y1="1760" y2="1760" x1="992" />
        </branch>
        <branch name="RX">
            <wire x2="624" y1="2384" y2="2384" x1="592" />
        </branch>
        <branch name="select_data_in">
            <wire x2="1216" y1="2592" y2="2592" x1="880" />
            <wire x2="1232" y1="2576" y2="2576" x1="1216" />
            <wire x2="1216" y1="2576" y2="2592" x1="1216" />
        </branch>
        <branch name="XLXN_215(7:0)">
            <wire x2="1952" y1="1136" y2="1136" x1="1872" />
            <wire x2="1872" y1="1136" y2="1248" x1="1872" />
            <wire x2="2144" y1="1248" y2="1248" x1="1872" />
            <wire x2="2144" y1="1248" y2="2368" x1="2144" />
            <wire x2="2288" y1="2368" y2="2368" x1="2144" />
            <wire x2="2080" y1="2768" y2="2768" x1="1936" />
            <wire x2="2144" y1="2368" y2="2368" x1="2080" />
            <wire x2="2080" y1="2368" y2="2768" x1="2080" />
            <wire x2="2288" y1="1904" y2="2368" x1="2288" />
            <wire x2="2512" y1="1904" y2="1904" x1="2288" />
        </branch>
        <branch name="XLXN_216">
            <wire x2="2032" y1="512" y2="512" x1="1744" />
            <wire x2="2080" y1="512" y2="512" x1="2032" />
            <wire x2="2032" y1="512" y2="704" x1="2032" />
            <wire x2="3280" y1="704" y2="704" x1="2032" />
            <wire x2="3280" y1="704" y2="1584" x1="3280" />
            <wire x2="3376" y1="1584" y2="1584" x1="3280" />
            <wire x2="3376" y1="1584" y2="2784" x1="3376" />
            <wire x2="3408" y1="2784" y2="2784" x1="3376" />
            <wire x2="2080" y1="480" y2="512" x1="2080" />
            <wire x2="2432" y1="480" y2="480" x1="2080" />
        </branch>
        <branch name="enable">
            <wire x2="848" y1="576" y2="576" x1="720" />
            <wire x2="992" y1="576" y2="576" x1="848" />
            <wire x2="848" y1="576" y2="752" x1="848" />
            <wire x2="1648" y1="752" y2="752" x1="848" />
            <wire x2="1840" y1="752" y2="752" x1="1648" />
            <wire x2="1648" y1="752" y2="1776" x1="1648" />
            <wire x2="2240" y1="1776" y2="1776" x1="1648" />
            <wire x2="2512" y1="1776" y2="1776" x1="2240" />
            <wire x2="2240" y1="1776" y2="2912" x1="2240" />
            <wire x2="3408" y1="2912" y2="2912" x1="2240" />
            <wire x2="1840" y1="608" y2="752" x1="1840" />
            <wire x2="2432" y1="608" y2="608" x1="1840" />
        </branch>
        <branch name="reset">
            <wire x2="832" y1="1968" y2="1968" x1="336" />
            <wire x2="832" y1="1968" y2="2048" x1="832" />
            <wire x2="336" y1="1968" y2="2848" x1="336" />
            <wire x2="3408" y1="2848" y2="2848" x1="336" />
            <wire x2="816" y1="640" y2="640" x1="720" />
            <wire x2="816" y1="640" y2="960" x1="816" />
            <wire x2="880" y1="640" y2="640" x1="816" />
            <wire x2="992" y1="640" y2="640" x1="880" />
            <wire x2="880" y1="640" y2="720" x1="880" />
            <wire x2="1760" y1="720" y2="720" x1="880" />
            <wire x2="720" y1="960" y2="1632" x1="720" />
            <wire x2="752" y1="1632" y2="1632" x1="720" />
            <wire x2="1024" y1="1632" y2="1632" x1="752" />
            <wire x2="752" y1="1632" y2="2048" x1="752" />
            <wire x2="832" y1="2048" y2="2048" x1="752" />
            <wire x2="816" y1="960" y2="960" x1="720" />
            <wire x2="1808" y1="1424" y2="1424" x1="752" />
            <wire x2="1808" y1="1424" y2="1840" x1="1808" />
            <wire x2="2512" y1="1840" y2="1840" x1="1808" />
            <wire x2="752" y1="1424" y2="1632" x1="752" />
            <wire x2="1760" y1="544" y2="720" x1="1760" />
            <wire x2="2432" y1="544" y2="544" x1="1760" />
            <wire x2="1952" y1="1072" y2="1072" x1="1808" />
            <wire x2="1808" y1="1072" y2="1424" x1="1808" />
        </branch>
        <branch name="XLXN_222(7:0)">
            <wire x2="1152" y1="2464" y2="2704" x1="1152" />
            <wire x2="1232" y1="2704" y2="2704" x1="1152" />
            <wire x2="1344" y1="2464" y2="2464" x1="1152" />
            <wire x2="1344" y1="2464" y2="2480" x1="1344" />
            <wire x2="2128" y1="2480" y2="2480" x1="1344" />
            <wire x2="2128" y1="1568" y2="1568" x1="1504" />
            <wire x2="2128" y1="1568" y2="2480" x1="2128" />
        </branch>
        <branch name="XLXN_223">
            <wire x2="1024" y1="1568" y2="1568" x1="1008" />
            <wire x2="1008" y1="1568" y2="1856" x1="1008" />
            <wire x2="1824" y1="1856" y2="1856" x1="1008" />
            <wire x2="1824" y1="624" y2="624" x1="1744" />
            <wire x2="1824" y1="624" y2="864" x1="1824" />
            <wire x2="1824" y1="864" y2="880" x1="1824" />
            <wire x2="1824" y1="880" y2="1856" x1="1824" />
            <wire x2="1952" y1="880" y2="880" x1="1824" />
            <wire x2="1888" y1="864" y2="864" x1="1824" />
            <wire x2="1888" y1="864" y2="1584" x1="1888" />
            <wire x2="2512" y1="1584" y2="1584" x1="1888" />
        </branch>
        <branch name="XLXN_231(3:0)">
            <wire x2="3456" y1="2560" y2="2560" x1="3088" />
            <wire x2="3456" y1="1808" y2="2560" x1="3456" />
            <wire x2="3824" y1="1808" y2="1808" x1="3456" />
        </branch>
        <branch name="XLXN_232(3:0)">
            <wire x2="3440" y1="2624" y2="2624" x1="3088" />
            <wire x2="3440" y1="1872" y2="2624" x1="3440" />
            <wire x2="3824" y1="1872" y2="1872" x1="3440" />
        </branch>
        <branch name="XLXN_233(3:0)">
            <wire x2="3424" y1="2688" y2="2688" x1="3088" />
            <wire x2="3424" y1="1936" y2="2688" x1="3424" />
            <wire x2="3824" y1="1936" y2="1936" x1="3424" />
        </branch>
        <branch name="XLXN_234(3:0)">
            <wire x2="3392" y1="2752" y2="2752" x1="3088" />
            <wire x2="3392" y1="2000" y2="2752" x1="3392" />
            <wire x2="3824" y1="2000" y2="2000" x1="3392" />
        </branch>
        <branch name="XLXN_235(1:0)">
            <wire x2="3760" y1="2160" y2="2160" x1="3712" />
            <wire x2="3712" y1="2160" y2="2208" x1="3712" />
            <wire x2="3712" y1="2208" y2="2592" x1="3712" />
            <wire x2="3840" y1="2592" y2="2592" x1="3712" />
            <wire x2="3840" y1="2592" y2="2784" x1="3840" />
            <wire x2="3840" y1="2208" y2="2208" x1="3712" />
            <wire x2="3824" y1="2064" y2="2064" x1="3760" />
            <wire x2="3760" y1="2064" y2="2160" x1="3760" />
        </branch>
        <instance x="2512" y="1936" name="XLXI_17" orien="R0">
        </instance>
        <branch name="XLXN_228">
            <wire x2="1744" y1="1056" y2="1056" x1="1552" />
            <wire x2="1744" y1="944" y2="1056" x1="1744" />
            <wire x2="1856" y1="944" y2="944" x1="1744" />
            <wire x2="1952" y1="944" y2="944" x1="1856" />
            <wire x2="1856" y1="944" y2="1648" x1="1856" />
            <wire x2="2512" y1="1648" y2="1648" x1="1856" />
        </branch>
        <branch name="XLXN_229">
            <wire x2="1760" y1="1248" y2="1248" x1="1552" />
            <wire x2="1760" y1="1008" y2="1248" x1="1760" />
            <wire x2="1904" y1="1008" y2="1008" x1="1760" />
            <wire x2="1952" y1="1008" y2="1008" x1="1904" />
            <wire x2="1904" y1="1008" y2="1712" x1="1904" />
            <wire x2="2512" y1="1712" y2="1712" x1="1904" />
        </branch>
        <branch name="FULL">
            <wire x2="3120" y1="1584" y2="1584" x1="3088" />
        </branch>
        <branch name="EMPTY">
            <wire x2="3120" y1="1648" y2="1648" x1="3088" />
        </branch>
        <branch name="XLXN_245(7:0)">
            <wire x2="2448" y1="2304" y2="2304" x1="2320" />
            <wire x2="2320" y1="2304" y2="2464" x1="2320" />
            <wire x2="2320" y1="2464" y2="2560" x1="2320" />
            <wire x2="2384" y1="2560" y2="2560" x1="2320" />
            <wire x2="3344" y1="2464" y2="2464" x1="2320" />
            <wire x2="3344" y1="1120" y2="1120" x1="2736" />
            <wire x2="3344" y1="1120" y2="2464" x1="3344" />
        </branch>
        <branch name="SEG(6:0)">
            <wire x2="5152" y1="1088" y2="1088" x1="5120" />
        </branch>
        <instance x="3840" y="2496" name="XLXI_29" orien="R0">
        </instance>
        <instance x="2448" y="2336" name="XLXI_30" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="112" y1="1904" y2="2992" x1="112" />
            <wire x2="128" y1="2992" y2="2992" x1="112" />
            <wire x2="176" y1="1904" y2="1904" x1="112" />
            <wire x2="240" y1="1904" y2="1904" x1="176" />
            <wire x2="176" y1="1744" y2="1840" x1="176" />
            <wire x2="240" y1="1840" y2="1840" x1="176" />
            <wire x2="240" y1="1840" y2="1856" x1="240" />
            <wire x2="464" y1="1744" y2="1744" x1="176" />
            <wire x2="240" y1="1856" y2="1856" x1="176" />
            <wire x2="176" y1="1856" y2="1904" x1="176" />
            <wire x2="464" y1="512" y2="512" x1="320" />
            <wire x2="992" y1="512" y2="512" x1="464" />
            <wire x2="464" y1="512" y2="1744" x1="464" />
        </branch>
        <branch name="XLXN_248">
            <wire x2="464" y1="1840" y2="1856" x1="464" />
            <wire x2="528" y1="1856" y2="1856" x1="464" />
            <wire x2="528" y1="1856" y2="1904" x1="528" />
            <wire x2="544" y1="1840" y2="1840" x1="464" />
            <wire x2="544" y1="1840" y2="2320" x1="544" />
            <wire x2="624" y1="2320" y2="2320" x1="544" />
            <wire x2="528" y1="1904" y2="1904" x1="464" />
        </branch>
        <instance x="240" y="1936" name="XLXI_32" orien="R0" />
        <iomarker fontsize="28" x="4128" y="320" name="DP" orien="R0" />
        <iomarker fontsize="28" x="4096" y="384" name="afficheur_0" orien="R0" />
        <iomarker fontsize="28" x="4112" y="448" name="afficheur_1" orien="R0" />
        <iomarker fontsize="28" x="4128" y="512" name="afficheur_2" orien="R0" />
        <iomarker fontsize="28" x="4144" y="592" name="afficheur_3" orien="R0" />
        <iomarker fontsize="28" x="4192" y="656" name="afficheur_4" orien="R0" />
        <iomarker fontsize="28" x="4176" y="704" name="afficheur_5" orien="R0" />
        <iomarker fontsize="28" x="4176" y="768" name="afficheur_6" orien="R0" />
        <iomarker fontsize="28" x="4160" y="816" name="afficheur_7" orien="R0" />
        <iomarker fontsize="28" x="4816" y="1808" name="SEG2(6:0)" orien="R0" />
        <iomarker fontsize="28" x="4416" y="2336" name="afficheur_9" orien="R0" />
        <iomarker fontsize="28" x="4416" y="2400" name="afficheur_10" orien="R0" />
        <iomarker fontsize="28" x="4416" y="2464" name="afficheur_11" orien="R0" />
        <iomarker fontsize="28" x="3440" y="1024" name="Commande_4_Phase(3:0)" orien="R0" />
        <iomarker fontsize="28" x="3456" y="960" name="Visu_DOWN_freq" orien="R0" />
        <iomarker fontsize="28" x="3216" y="912" name="Visu_UP_freq" orien="R0" />
        <iomarker fontsize="28" x="912" y="1056" name="bouton_UP" orien="R180" />
        <iomarker fontsize="28" x="912" y="1120" name="bouton_DOWN" orien="R180" />
        <iomarker fontsize="28" x="992" y="1696" name="rotary_A" orien="R180" />
        <iomarker fontsize="28" x="992" y="1760" name="rotary_B" orien="R180" />
        <iomarker fontsize="28" x="592" y="2384" name="RX" orien="R180" />
        <iomarker fontsize="28" x="720" y="576" name="enable" orien="R180" />
        <iomarker fontsize="28" x="720" y="640" name="reset" orien="R180" />
        <iomarker fontsize="28" x="3120" y="1584" name="FULL" orien="R0" />
        <iomarker fontsize="28" x="3120" y="1648" name="EMPTY" orien="R0" />
        <iomarker fontsize="28" x="5152" y="1088" name="SEG(6:0)" orien="R0" />
        <iomarker fontsize="28" x="4432" y="2272" name="afficheur_8" orien="R0" />
        <iomarker fontsize="28" x="320" y="512" name="clk" orien="R180" />
        <instance x="1232" y="2800" name="XLXI_34" orien="R0">
        </instance>
        <branch name="XLXN_249">
            <wire x2="944" y1="1184" y2="1184" x1="880" />
            <wire x2="880" y1="1184" y2="1344" x1="880" />
            <wire x2="2160" y1="1344" y2="1344" x1="880" />
            <wire x2="2160" y1="1344" y2="2048" x1="2160" />
            <wire x2="2000" y1="2576" y2="2576" x1="1936" />
            <wire x2="2000" y1="2048" y2="2576" x1="2000" />
            <wire x2="2160" y1="2048" y2="2048" x1="2000" />
        </branch>
        <branch name="XLXN_250">
            <wire x2="944" y1="1248" y2="1248" x1="896" />
            <wire x2="896" y1="1248" y2="1328" x1="896" />
            <wire x2="2112" y1="1328" y2="1328" x1="896" />
            <wire x2="2112" y1="1328" y2="2208" x1="2112" />
            <wire x2="1936" y1="2208" y2="2496" x1="1936" />
            <wire x2="1984" y1="2496" y2="2496" x1="1936" />
            <wire x2="1984" y1="2496" y2="2672" x1="1984" />
            <wire x2="2112" y1="2208" y2="2208" x1="1936" />
            <wire x2="1984" y1="2672" y2="2672" x1="1936" />
        </branch>
        <branch name="XLXN_251(7:0)">
            <wire x2="416" y1="2256" y2="3120" x1="416" />
            <wire x2="560" y1="3120" y2="3120" x1="416" />
            <wire x2="1120" y1="2256" y2="2256" x1="416" />
            <wire x2="1120" y1="2256" y2="2384" x1="1120" />
            <wire x2="1120" y1="2384" y2="2768" x1="1120" />
            <wire x2="1232" y1="2768" y2="2768" x1="1120" />
            <wire x2="1120" y1="2384" y2="2384" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="880" y="2592" name="select_data_in" orien="R180" />
        <branch name="select_CW_or_CCW(1:0)">
            <wire x2="1216" y1="2688" y2="2688" x1="1072" />
            <wire x2="1232" y1="2640" y2="2640" x1="1216" />
            <wire x2="1216" y1="2640" y2="2688" x1="1216" />
        </branch>
        <iomarker fontsize="28" x="1072" y="2688" name="select_CW_or_CCW(1:0)" orien="R180" />
        <instance x="128" y="3024" name="XLXI_39" orien="R0" />
        <branch name="XLXN_261">
            <wire x2="560" y1="2992" y2="2992" x1="352" />
        </branch>
        <branch name="XLXN_265">
            <wire x2="432" y1="2496" y2="3056" x1="432" />
            <wire x2="560" y1="3056" y2="3056" x1="432" />
            <wire x2="1408" y1="2496" y2="2496" x1="432" />
            <wire x2="1184" y1="2320" y2="2320" x1="1008" />
            <wire x2="1184" y1="2176" y2="2320" x1="1184" />
            <wire x2="1408" y1="2176" y2="2176" x1="1184" />
            <wire x2="1408" y1="2176" y2="2496" x1="1408" />
        </branch>
        <instance x="560" y="3152" name="XLXI_44" orien="R0">
        </instance>
        <branch name="TX_active">
            <wire x2="976" y1="2992" y2="2992" x1="944" />
        </branch>
        <iomarker fontsize="28" x="976" y="2992" name="TX_active" orien="R0" />
        <branch name="TX">
            <wire x2="976" y1="3056" y2="3056" x1="944" />
        </branch>
        <iomarker fontsize="28" x="976" y="3056" name="TX" orien="R0" />
        <branch name="TX_clone">
            <wire x2="976" y1="3120" y2="3120" x1="944" />
        </branch>
        <iomarker fontsize="28" x="976" y="3120" name="TX_clone" orien="R0" />
    </sheet>
</drawing>