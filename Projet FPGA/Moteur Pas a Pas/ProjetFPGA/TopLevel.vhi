-- Vhdl instantiation template created from schematic C:\Users\pivot\Desktop\Projet FPGA\Moteur Pas a Pas\ProjetFPGA\TopLevel.sch - Sat Mar 06 19:31:32 2021
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module.
-- 2) To use this template to instantiate this component, cut-and-paste and then edit.
--

   COMPONENT TopLevel
   PORT( SEG2	:	OUT	STD_LOGIC_VECTOR (6 DOWNTO 0); 
          DP	:	OUT	STD_LOGIC; 
          afficheur_0	:	OUT	STD_LOGIC; 
          Visu_UP_freq	:	OUT	STD_LOGIC; 
          Visu_DOWN_freq	:	OUT	STD_LOGIC; 
          Commande_4_Phase	:	OUT	STD_LOGIC_VECTOR (3 DOWNTO 0); 
          afficheur_1	:	OUT	STD_LOGIC; 
          afficheur_2	:	OUT	STD_LOGIC; 
          afficheur_3	:	OUT	STD_LOGIC; 
          afficheur_4	:	OUT	STD_LOGIC; 
          afficheur_5	:	OUT	STD_LOGIC; 
          afficheur_6	:	OUT	STD_LOGIC; 
          afficheur_7	:	OUT	STD_LOGIC; 
          afficheur_8	:	OUT	STD_LOGIC; 
          afficheur_9	:	OUT	STD_LOGIC; 
          afficheur_10	:	OUT	STD_LOGIC; 
          afficheur_11	:	OUT	STD_LOGIC; 
          bouton_UP	:	IN	STD_LOGIC; 
          bouton_DOWN	:	IN	STD_LOGIC; 
          rotary_A	:	IN	STD_LOGIC; 
          rotary_B	:	IN	STD_LOGIC; 
          RX	:	IN	STD_LOGIC; 
          select_data_in	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
          enable	:	IN	STD_LOGIC; 
          reset	:	IN	STD_LOGIC; 
          FULL	:	OUT	STD_LOGIC; 
          EMPTY	:	OUT	STD_LOGIC; 
          SEG	:	OUT	STD_LOGIC_VECTOR (6 DOWNTO 0));
   END COMPONENT;

   UUT: TopLevel PORT MAP(
		SEG2 => , 
		DP => , 
		afficheur_0 => , 
		Visu_UP_freq => , 
		Visu_DOWN_freq => , 
		Commande_4_Phase => , 
		afficheur_1 => , 
		afficheur_2 => , 
		afficheur_3 => , 
		afficheur_4 => , 
		afficheur_5 => , 
		afficheur_6 => , 
		afficheur_7 => , 
		afficheur_8 => , 
		afficheur_9 => , 
		afficheur_10 => , 
		afficheur_11 => , 
		bouton_UP => , 
		bouton_DOWN => , 
		rotary_A => , 
		rotary_B => , 
		RX => , 
		select_data_in => , 
		clk => , 
		enable => , 
		reset => , 
		FULL => , 
		EMPTY => , 
		SEG => 
   );
