library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Top_level_module_moteur_pas_a_pas is
    Port (
        bouton_DOWN            : in STD_LOGIC;
        bouton_UP              : in STD_LOGIC;
        CLK                    : in STD_LOGIC;
        reset                  : in STD_LOGIC;
        enable                 : in STD_LOGIC;
        rotary_A               : in STD_LOGIC;
        rotary_B               : in STD_LOGIC;
        select_freq_or_vitesse : in STD_LOGIC;
        afficheur_0            : out STD_LOGIC;
        afficheur_1            : out STD_LOGIC;
        afficheur_2            : out STD_LOGIC;
        afficheur_3            : out STD_LOGIC;
        afficheur_4            : out STD_LOGIC;
        afficheur_5            : out STD_LOGIC;
        afficheur_6            : out STD_LOGIC;
        afficheur_7            : out STD_LOGIC;
        Commande_4_Phase       : out STD_LOGIC_VECTOR (3 downto 0);
        DP                     : out STD_LOGIC;
        EMPTY                  : out STD_LOGIC;
        FULL                   : out STD_LOGIC;
        SEG                    : out STD_LOGIC_VECTOR (6 downto 0);
        Visu_DOWN_freq         : out STD_LOGIC;
        Visu_UP_freq           : out STD_LOGIC);

end Top_level_module_moteur_pas_a_pas;

architecture Behavioral of Top_level_module_moteur_pas_a_pas is
-- signaux à declarer--
        signal sortie_mux : std_logic_vector (3 downto 0);
        signal A          : std_logic_vector (3 downto 0);
        signal B          : std_logic_vector (3 downto 0);
        signal C          : std_logic_vector (3 downto 0);
        signal D          : std_logic_vector (3 downto 0);
        signal E          : std_logic_vector (3 downto 0);
        signal F          : std_logic_vector (3 downto 0);
        signal G          : std_logic_vector (3 downto 0);
        signal H          : std_logic_vector (3 downto 0);
        signal SEL                       : std_logic_vector (2 downto 0);
        signal change_frequency_motor    : std_logic_vector (7 downto 0);
        signal clk_synchro_affichage     : std_logic := '0';
        signal clk_synchro_state_machine : std_logic := '0';
        signal clk_div1                  : std_logic := '0';
        signal clk_div2                  : std_logic := '0';
        signal frequency_in_motor        : std_logic_vector (7 downto 0);
-- on declare les composants ou les modules--
-- component BUFG
-- port (
-- I : in std_logic;
-- O : out std_logic);
-- end component;
    component BCD_7Segments
        port ( 
            d : in std_logic_vector (3 downto 0);
            s : out std_logic_vector (6 downto 0));
    end component
    
    component MUX
        port (
            A           : in std_logic_vector (3 downto 0);
            B           : in std_logic_vector (3 downto 0);
            C           : in std_logic_vector (3 downto 0);
            D           : in std_logic_vector (3 downto 0);
            E           : in std_logic_vector (3 downto 0);
            F           : in std_logic_vector (3 downto 0);
            G           : in td_logic_vector (3 downto 0);
            H           : in std_logic_vector (3 downto 0);
            SEL         : in std_logic_vector (2 downto 0);
            sortie_mux  : out std_logic_vector (3 downto 0));
    end component;
    
    component compteur_3bits
        port ( 
            clk             : in std_logic;
            reset           : in std_logic;
            enable          : in std_logic;
            sortie_compteur : out std_logic_vector (2 downto 0));
    end component;

    component calcul_vitesse_stepper_motor
        port (
            select_freq_or_vitesse   : in std_logic;
            frequency_in_motor       : in std_logic_vector (7 downto 0);
            affichage_vitesse_digit0 : out std_logic_vector (3 downto 0);
            affichage_vitesse_digit1 : out std_logic_vector (3 downto 0);
            affichage_vitesse_digit2 : out std_logic_vector (3 downto 0);
            affichage_vitesse_digit3 : out std_logic_vector (3 downto 0));
    end component;
    
    component state_machine_stepper_motor
        port ( 
            clk         : in std_logic;
            bouton_UP   : in std_logic;
            bouton_DOWN : in std_logic;
            reset       : in std_logic;
            change_frequency_motor      : in std_logic_vector (7 downto 0);
            visu_UP                     : out std_logic;
            visu_DOWN                   : out std_logic;
            Commandes_demi_pas          : out std_logic_vector (3 downto 0);
            vers_change_frequency_motor : out std_logic_vector (7 downto 0));
    end component;

    component DECODE_2_to_8
        port (
             select_freq_or_vitesse : in std_logic;
             SEL                    : in std_logic_vector (2 downto 0);
             DP1                    : out std_logic;
             afficheur_0 : out std_logic;
             afficheur_1 : out std_logic;
             afficheur_2 : out std_logic;
             afficheur_3 : out std_logic;
             afficheur_4 : out std_logic;
             afficheur_5 : out std_logic;
             afficheur_6 : out std_logic;
             afficheur_7 : out std_logic);
    end component;

    component CompteurBCD
        port (
            CLK                    : in std_logic;
            bouton_UP              : in std_logic;
            bouton_DOWN            : in std_logic;
            Enable                 : in std_logic;
            Reset                  : in std_logic;
            change_frequency_motor : in std_logic_vector (7 downto 0);
            Full    : out std_logic;
            Empty   : out std_logic;
            BCD_U   : out std_logic_vector (3 downto 0);
            BCD_D   : out std_logic_vector (3 downto 0);
            BCD_H   : out std_logic_vector (3 downto 0);
            BCD_T   : out std_logic_vector (3 downto 0));
    end component;

    component clock_manager_project
        port ( 
            clk      : in std_logic;
            ce       : in std_logic;
            reset    : in std_logic;
            clk_div1 : out std_logic;
            clk_div2 : out std_logic);
    end component;

    component codeur_numerique
        port (
            CLK               : in std_logic;
            reset             : in std_logic;
            rotary_A          : in std_logic;
            rotary_B          : in std_logic;
            compte_out_codeur : out std_logic_vector (7 downto 0));
    end component;
    
    --instanciation : on associe les modules ou composants entre eux—
    begin
        -- BUFG: Global Clock Simple Buffer
        -- 7 Series
        -- Xilinx HDL Libraries Guide, version 14.1
        
        BUFG_inst1 : BUFG
            port map (
                O => clk_synchro_affichage, -- 1-bit output: Clock output
                I => clk_div1 -- 1-bit input: Clock input
            );
        -- End of BUFG_inst instantiation
        -- BUFG: Global Clock Simple Buffer
        -- 7 Series
        -- Xilinx HDL Libraries Guide, version 14.1
        
        BUFG_inst2 : BUFG
            port map (
                O=>clk_synchro_state_machine, -- 1-bit output: Clock output
                I=>clk_div2 -- 1-bit input: Clock inp
            );
        -- End of BUFG_inst instantiation
        
        module_3 : BCD_7Segments
            port map (
                d(3 downto 0)=>sortie_mux(3 downto 0),
                s(6 downto 0)=>SEG(6 downto 0)
            );

        module_4 : MUX
            port map (
                A(3 downto 0)=>A(3 downto 0),
                B(3 downto 0)=>B(3 downto 0),
                C(3 downto 0)=>C(3 downto 0),
                D(3 downto 0)=>D(3 downto 0),
                E(3 downto 0)=>E(3 downto 0),
                F(3 downto 0)=>F(3 downto 0),
                G(3 downto 0)=>G(3 downto 0),
                H(3 downto 0)=>H(3 downto 0),
                SEL(2 downto 0)=>SEL(2 downto 0),
                sortie_mux(3 downto 0)=>sortie_mux(3 downto 0)
            );

        module_5 : compteur_3bits
            port map (
                clk=>clk_synchro_affichage,
                enable=>enable,
                reset=>reset,
                sortie_compteur(2 downto 0)=>SEL(2 downto 0)
            );
    
        module_6 : calcul_vitesse_stepper_motor
            port map (
                frequency_in_motor(7 downto 0)=>frequency_in_motor(7 downto 0),
                select_freq_or_vitesse=>select_freq_or_vitesse,
                affichage_vitesse_digit0(3 downto 0)=>E(3 downto 0),
                affichage_vitesse_digit1(3 downto 0)=>F(3 downto 0),
                affichage_vitesse_digit2(3 downto 0)=>G(3 downto 0),
                affichage_vitesse_digit3(3 downto 0)=>H(3 downto 0)
            );

        module_7 : state_machine_stepper_motor
            port map (
                bouton_DOWN=>bouton_DOWN,
                bouton_UP=>bouton_UP,
                change_frequency_motor(7 downto 0)=>change_frequency_motor(7 downto 0),
                clk=>clk_synchro_state_machine,
                reset=>reset,
                Commandes_demi_pas(3 downto 0)=>Commande_4_Phase(3 downto 0),
                vers_change_frequency_motor(7 downto 0)=>frequency_in_motor(7 downto 0),
                visu_DOWN=>Visu_DOWN_freq,
                visu_UP=>Visu_UP_freq
            );
            
        module_8 : DECODE_2_to_8
            port map (
                SEL(2 downto 0)=>SEL(2 downto 0),
                select_freq_or_vitesse=>select_freq_or_vitesse,
                afficheur_0=>afficheur_0,
                afficheur_1=>afficheur_1,
                afficheur_2=>afficheur_2,
                afficheur_3=>afficheur_3,
                afficheur_4=>afficheur_4,
                afficheur_5=>afficheur_5,
                afficheur_6=>afficheur_6,
                afficheur_7=>afficheur_7,
                DP1=>DP
            );

        module_9 : CompteurBCD
            port map (
                bouton_DOWN=>bouton_DOWN,
                bouton_UP=>bouton_UP,
                change_frequency_motor(7 downto 0)=>change_frequency_motor(7 downto 0),
                CLK=>clk_synchro_state_machine,
                Enable=>enable,
                Reset=>reset,
                BCD_D(3 downto 0)=>B(3 downto 0),
                BCD_H(3 downto 0)=>C(3 downto 0),
                BCD_T(3 downto 0)=>D(3 downto 0),
                BCD_U(3 downto 0)=>A(3 downto 0),
                Empty=>EMPTY,
                Full=>FULL
            );
            
        module_10 : clock_manager_project
            port map (
                ce=>enable,
                clk=>clk,
                reset=>reset,
                clk_div1=>clk_div1,
                clk_div2=>clk_div2
            );

        module_11 : codeur_numerique
            port map (
                CLK=>clk_synchro_state_machine,
                reset=>reset,
                rotary_A=>rotary_A,
                rotary_B=>rotary_B,
                compte_out_codeur(7 downto 0)=>change_frequency_motor(7 downto 0)
            );
end Behavioral;