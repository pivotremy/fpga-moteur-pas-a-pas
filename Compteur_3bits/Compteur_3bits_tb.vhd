
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY compteur_3bits_tb IS
END compteur_3bits_tb;
 
ARCHITECTURE behavior OF compteur_3bits_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT compteur_3bits
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         enable : IN  std_logic;
         sortie_compteur : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal enable : std_logic := '0';

 	--Outputs
   signal sortie_compteur : std_logic_vector(2 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: compteur_3bits PORT MAP (
          clk => clk,
          reset => reset,
          enable => enable,
          sortie_compteur => sortie_compteur
        );

   -- Clock process definitions
   reset_process :process
   begin
		reset <= '1';
		wait for 15 ns;
		reset <= '0';
		wait ;
   end process;
 
    clk_process :process
   begin
		clk <= '0';
		wait for 5 ns;
		clk <= '1';
		wait for 5 ns;
   end process;

	enable_process :process
   begin
		enable <= '0';
		wait for 5 ns;
		enable <= '1';
		wait for 100 ns;
		enable <= '0';
		wait for 15 ns;
		enable <= '1';
		wait ;
   end process;
END;