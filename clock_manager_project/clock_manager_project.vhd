library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity clock_manager_project is
    port (
        clk : in std_logic ; --100MHz
        clk_div1 : out std_logic ; --1000Hz
        clk_div2 : out std_logic ; --1000Hz
        ce       : in std_logic;
        reset    : in std_logic
    );
end clock_manager_project;

architecture Behavioral of clock_manager_project is

    signal count1 : integer range 0 to 100000 := 0;
    signal clock_int1 : std_logic := '0';
    signal count2 : integer range 0 to 100000 := 0;
    signal clock_int2 : std_logic :='0';
    constant M1 : integer := 100000 ; -- resultat de la division pour 1000Hz
    constant M2 : integer := 100000 ; -- resultat de la division pour 1000Hz

begin
    -- Divise par 10000 FOUT=1000Hz synchro affichage et synchronisation state machine
    process(clk,ce,reset)
    begin
        -- code a completer
        if reset ='1' then
            count1 <= 0;
            count2 <= 0;
        elsif rising_edge(clk) then
            if ce ='1' then
			    if count1 < M1 then count1 <= count1 + 1; else count1 <= 0; end if; 
             if count2 < M2 then count2 <= count2 + 1; else count2 <= 0; end if;
			end if;
        end if;
    end process;
	 clock_int1 <= '1' WHEN count1 <= M1/2 ELSE '0';
	 clk_div1 <= clock_int1;
	 clock_int2 <= '1' WHEN count1 <= M2/2 ELSE '0';
	 clk_div2 <= clock_int2;
end Behavioral;